import { useEffect, useState } from 'react'
import { createTheme } from '@mui/material';
import { Box, ThemeProvider } from '@mui/system';
import AppBar from './app/MenuBar';
import Viewport from './features/viewport/Viewport';
import RobotList from './features/robots/RobotList';
import { Split } from '@geoffcox/react-splitter';
import { useStore } from './common/store';
import Viewport2D from './features/viewport/Viewport2D';
import AreaOfInterests from './features/aoi/AreaOfInterests';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

const lightTheme = createTheme({
  palette: {
    mode: 'light',
  },
});

function Desktop() {
  const updateMessageRates = useStore(state => state.updateMessageRates);
  const darkMode = useStore(state => state.appSettings.darkTheme);

  useEffect(
    () => {
      const interval = setInterval(() => updateMessageRates(1), 1000);
      return () => clearInterval(interval);
    },
    [updateMessageRates]
  );

  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <Box
        component="div"
        sx={{
          height: '100%',
          minHeight: 0,
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        <AppBar />
        <Split
          initialPrimarySize='20%'
        >
          <RobotList />
          <Split
            initialPrimarySize='75%'
          >
            <Split
              horizontal
            >
              <Viewport />
              <Viewport2D />
            </Split>
            <AreaOfInterests />
          </Split>
        </Split>
      </Box>
    </ThemeProvider>
  );
}

export default Desktop;
