import { useEffect } from "react";

export function useSocket(url: string, onmessage: (this: WebSocket, ev: MessageEvent) => any | null, onopen?: ((this: WebSocket, ev: Event) => any) | null, onclose?: ((this: WebSocket, ev: CloseEvent) => any) | null, onerror?: ((this: WebSocket, ev: Event) => any) | null) {
    useEffect(() => {
        let socket = new WebSocket("wss://" + url);
        socket.onmessage = onmessage;
        socket.onopen = onopen || null;
        socket.onerror = onerror || null;
        socket.onclose = onclose || null;
        return () => {
            socket.close();
        }
    }, [url]);
}