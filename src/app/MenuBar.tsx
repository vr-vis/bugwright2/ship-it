import { AppBar as MUIAppBar, IconButton, Switch } from '@mui/material';
import { DarkMode, LightMode, ViewInAr } from '@mui/icons-material';
import Grid2 from '@mui/material/Unstable_Grid2';
import CameraMenu from '../features/camera/CameraMenu';
import ConnectionMenu from '../features/connections/ConnectionsMenu';
import { Link } from 'react-router-dom';
import { useStore } from '../common/store';
import AppSettingsPopover from '../features/app/AppSettingsPopover';

export interface AppBarProps {
}

function AppBar(props: AppBarProps) {
  return (
    <MUIAppBar position="sticky">
      <Grid2 container>
        <Grid2>
          <ConnectionMenu />
          {/* <CameraMenu /> */}
          <IconButton
            component={Link}
            to="/?vr"
            color="inherit"
            title="Switch to immersive view"
          >
            <ViewInAr />
          </IconButton>
        </Grid2>
        <Grid2 xs>
        </Grid2>
        <Grid2>
          <AppSettingsPopover />

        </Grid2>
      </Grid2>
    </MUIAppBar>
  );
}

export default AppBar;
