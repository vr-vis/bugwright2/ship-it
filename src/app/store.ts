import { configureStore } from '@reduxjs/toolkit';
import configReducer from '../features/config/config-slice';
import cameraReducer from '../features/camera/camera-slice';
import connectionsReducer from '../features/connections/rosbridge-connections-slice';

export const store = configureStore({ 
  reducer: {
    config: configReducer,
    connections: connectionsReducer,
    camera: cameraReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
