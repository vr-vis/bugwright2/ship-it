import { useEffect, useMemo } from 'react';
import { validate } from 'jsonschema';
import ConfigSchema from './config-schemas/Config.schema.json';
import { Config } from './schemas/Config.schema';
import RobotSchema from './config-schemas/Robot.schema.json';
import { Robot } from './schemas/Robot.schema';
import { useStore } from './common/store';
import Desktop from './Desktop';
import Immersive from './Immersive';
import { useLocation } from 'react-router';

function useQuery() {
  const { search } = useLocation();

  return useMemo(() => new URLSearchParams(search), [search]);
}

function App() {
  const addRobot = useStore(state => state.addRobot);
  const setShip = useStore(state => state.setShip);
  const setTransformTree = useStore(state => state.setTransformTree);

  const query = useQuery();

  let isImmersive = query.get('vr') !== null;
  let config = query.get('config');

  useEffect(
    () => {
      if (config) {
        const configPath = config;
        let cancelled = false;

        const fetchConfig = async () => {
          const configResponse = await fetch(configPath);
          const json = await configResponse.json();
          validate(json, ConfigSchema, { throwAll: true });
          const config = json as Config;

          if (cancelled) {
            return;
          }

          setShip(config.ship);
          setTransformTree(config.transformTree);

          for (const robot of config.robots) {
            const robotResponse = await fetch(`${configPath.substring(0, configPath.lastIndexOf('/'))}/${robot}`);
            const json = await robotResponse.json();
            validate(json, RobotSchema, { throwAll: true });
            const robotConfig = json as Robot;
            if (!cancelled) {
              addRobot(robotConfig);
            }
          }
        }

        fetchConfig().catch(console.error);

        return () => { cancelled = true };
      }
    },
    [addRobot, setShip, config]
  );

  return (
    isImmersive ? <Immersive /> : <Desktop />
  );
}

export default App;
