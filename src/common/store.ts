import { useCallback, useEffect, useState } from 'react';
import { Ros, Service, Topic } from 'roslib';
import { Object3D } from 'three';
import create from 'zustand';
import { Config } from '../schemas/Config.schema';
import { Robot } from '../schemas/Robot.schema';
import { Messages } from "./ROS";

export interface ROSTopicState {
  topic: Topic;
  name: string;
  type: string;
  subscriberCount: number;
  updateCallback: (value: any) => void;
  value: any;
  messageCount: number;
  messageRate: number;
}

export interface RosbridgeConnectionState {
  uri: string;
  bsonMode: boolean;
  status: 'establishing connection' | 'connected' | 'disconnected';
  rosbridge: Ros;
  topics: {[topic: string]: ROSTopicState | number};
}

export type AreaOfInterestType = "biofouling" | "corrosion" | "metal-plate-deficiency" | "other";

export interface AreaOfInterest {
  id: number,
  type: AreaOfInterestType,
  location: [number, number, number],
  severity: number,
  note: string,
  nextInspection: string,
  audioRecordings: Blob[],
}

export interface AppSettings {
  showTFDebugVis: boolean,
  showTFDebugLabels: boolean,
  darkTheme: boolean,
  showSea: boolean,
  oceanLevelOffset: number,
  showSkybox: boolean,
}

export interface ConfigState {
  connections: {[uri: string]: RosbridgeConnectionState};
  ship: Config['ship'],
  transformTree: Config['transformTree'] | undefined,
  robots: {[name: string]: Robot};
  aois: {[id: string]: AreaOfInterest},
  appSettings: AppSettings,

  updateAppSettings: (newSettings: Partial<AppSettings>) => void;

  connect: (uri: string, bsonMode: boolean) => void;
  updateTopics: (uri: string) => void;

  setShip: (ship: Config['ship']) => void;
  setTransformTree: (ship: Config['transformTree']) => void;
  addRobot: (robot: Robot) => void;
  setAreaOfInterest: (aoi: AreaOfInterest) => void;

  subscribe: (uri: string, topic: string) => void;
  unsubscribe: (uri: string, topic: string) => void;
  updateMessageRates: (elapsedTime: number) => void;
}

export const useStore = create<ConfigState>((set, get) => {

  const createSubscriptionHandler = (uri: string, topic: string) => {
    return (value: any) => {
      // console.info(`Updating topic ${topic} of ${uri} to ${JSON.stringify(value)}`);
      set(state => {
        const topicState = state.connections[uri].topics[topic];
        if (typeof topicState === 'number') {
          console.error(`Recevied a message for a topic not subscribed to!`);
          return {};
        // } else {
        //   console.info(`Updating topic ${topic} of ${uri} to ${JSON.stringify(value)}`);
        }

        return {
          connections: {
            ...state.connections,
            [uri]: {
              ...state.connections[uri],
              topics: {
                ...state.connections[uri].topics,
                [topic]: {
                  ...topicState,
                  messageCount: topicState.messageCount + 1,
                  value,
                }
              }
            }
          }
        }
      });
    }
  };

  return {
    robots: {},
    connections: {},
    ship: undefined,
    transformTree: undefined,
    appSettings: {
      darkTheme: true,
      showSea: true,
      oceanLevelOffset: 0,
      showSkybox: true,
      showTFDebugLabels: true,
      showTFDebugVis: true
    },
    aois: {
      "0": {
        id: 0,
        type: "corrosion",
        severity: 5,
        location: [0, 0, 0],
        nextInspection: '',
        note: 'corrision',
        audioRecordings: [],
      },
      "1": {
        id: 1,
        type: "other",
        severity: 6,
        location: [0, 19, 0],
        nextInspection: '',
        note: 'corrision',
        audioRecordings: [],
      },
    },
    
    updateAppSettings: (newSettings: Partial<AppSettings>) => {
      set(state => ({ appSettings: {
        ...state.appSettings,
        ...newSettings
      }}));
    },

    connect: (uri, bsonMode) => {
      if (!(uri in get().connections)) {
        console.log(`Connecting to ${uri}`);
        const rosbridge = new Ros({ url: uri });

        set(state => ({
          connections: {
            ...state.connections,
            [uri]: {
              uri,
              bsonMode,
              status: 'establishing connection',
              topics: {},
              rosbridge,
            }
          }
        }));

        rosbridge.on('connection', _ => {
          set(state => ({
            connections: {
              ...state.connections,
              [uri]: {
                ...state.connections[uri],
                status: 'connected',
              }
            }
          }));
        });

        rosbridge.on('close', _ => {
          set(state => ({
            connections: {
              ...state.connections,
              [uri]: {
                ...state.connections[uri],
                status: 'disconnected',
              }
            }
          }));
        });

        get().updateTopics(uri);
      }
    },

    updateTopics: (uri: string) => {
      const connection = get().connections[uri];
      if (!connection) {
        console.error(`No connection to ${uri} exists`);
        return;
      }

      connection.rosbridge.getTopics(({ topics, types }) => {
        if (topics.length !== types.length) {
          console.error(`Invalid response from rosbridge`);
          return;
        }

        set(state => {
          const oldTopics = state.connections[uri].topics;
          const connectionTopics: {[topic: string]: ROSTopicState} = {};

          for (let i = 0; i < topics.length; ++i) {
            const oldTopic = oldTopics[topics[i]];
            if (typeof oldTopic === 'number' || typeof oldTopic === 'undefined') {
              const handler = createSubscriptionHandler(uri, topics[i]);
              const topicState = {
                name: topics[i],
                type: types[i],
                value: null,
                subscriberCount: oldTopic || 0,
                topic: new Topic({
                  ros: connection.rosbridge,
                  name: topics[i],
                  messageType: types[i],
                }),
                updateCallback: handler,
                messageCount: 0,
                messageRate: 0,
              };
              console.log(`Discovered ${topicState.name} on ${uri} (${oldTopic})`);
              if (oldTopic > 0) {
                console.log(`Subscribe to ${topicState.name} on ${uri}`);
                topicState.topic.subscribe(handler);
              }
              connectionTopics[topics[i]] = topicState;
            } else {
              console.warn(`Not implemented yet! ${topics[i]}`);
            }
          }

          return {
            connections: {
              ...state.connections,
              [uri]: {
                ...state.connections[uri],
                topics: connectionTopics,
              }
            }
          };
        });
      });
    },

    setShip: ship => {
      if (ship) {
        if (!(ship.rosbridge.uri in get().connections)) {
          get().connect(ship.rosbridge.uri, ship.rosbridge.bsonMode || false);
        }
      }
      set({ ship });
    },
    
    setTransformTree: tree => {
      if (tree) {
        if (!(tree.rosbridge.uri in get().connections)) {
          get().connect(tree.rosbridge.uri, tree.rosbridge.bsonMode || false);
        }
      }
      set({ transformTree : tree });
    },

    addRobot: (robot: Robot) => {
      if (!(robot.name in get().robots)) {
        if (!(robot.rosbridge.uri in get().connections)) {
          get().connect(robot.rosbridge.uri, robot.rosbridge.bsonMode || false);
        }
        set(state => ({ robots: {
          ...state.robots,
          [robot.name]: structuredClone(robot),
        }}));
      }
    },

    setAreaOfInterest: (aoi: AreaOfInterest) => {
      set(state => ({ aois: {
        ...state.aois,
        [aoi.id]: structuredClone(aoi),
      }}));
    },

    subscribe: (uri: string, topicName: string) => {
      const connection = get().connections[uri];
      if (!connection) {
        console.error(`Connection with uri ${uri} not found`);
        return;
      }

      const topic = connection.topics[topicName];
      if (!topic) {
        console.info(`Registered ${topicName} on ${uri} for subscription`);
        set(state => ({
          connections: {
            ...state.connections,
            [uri]: {
              ...state.connections[uri],
              topics: {
                ...state.connections[uri].topics,
                [topicName]: 1,
              }
            }
          }
        }));
      } else if (typeof topic === 'number') {
        set(state => ({
          connections: {
            ...state.connections,
            [uri]: {
              ...state.connections[uri],
              topics: {
                ...state.connections[uri].topics,
                [topicName]: topic + 1,
              }
            }
          }
        }));
      } else {
        if (topic.subscriberCount === 0) {
          console.log(`Subscribe to ${topicName} on ${uri}`);
          topic.topic.subscribe(topic.updateCallback);
        }
        topic.subscriberCount++;
        console.log(`Increased subscribers of ${topicName} on ${uri} to ${topic.subscriberCount}`);

        set(state => ({
          connections: {
            [uri]: {
              ...state.connections[uri],
              topics: {
                ...state.connections[uri].topics,
                [topicName]: topic,
              }
            }
          }
        }));
      }
    },

    unsubscribe: (uri: string, topicName: string) => {
      const connection = get().connections[uri];
      if (!connection) {
        console.error(`Connection with uri ${uri} not found`);
        return;
      }

      const topic = connection.topics[topicName];

      if (typeof topic !== 'object') {
        console.error(`Trying to unsubscribe from a topic not subscribed to!`);
        return;
      }

      topic.subscriberCount--;
      console.log(`Decreased subscribers of ${topicName} on ${uri} to ${topic.subscriberCount}`);
      if (topic.subscriberCount === 0) {
        console.log(`Unsubscribe from ${topicName} on ${uri}`);
        topic.topic.unsubscribe(topic.updateCallback);
      }
      set(state => ({
        connections: {
          [uri]: {
            ...state.connections[uri],
            topics: {
              ...state.connections[uri].topics,
              [topicName]: topic,
            }
          }
        }
      }));
    },

    updateMessageRates: (elapsedTime: number) => {
      set(state => {
        for (const connection of Object.values(state.connections)) {
          for (const topic of Object.values(connection.topics)) {
            if (typeof topic === 'object') {
              // if (topic.messageCount > 0) {
              //   console.log(`Topic ${topic.name} received ${topic.messageCount} messages in the last ${elapsedTime} seconds`);
              // }
              topic.messageRate = topic.messageCount / elapsedTime;
              topic.messageCount = 0;
            }
          }
        }
        return {
          connections: { ...state.connections }
        }
      });
    }
  }
});

export function useConnection(uri?: string) {
  return useStore(
    useCallback(
      state => uri ? state.connections[uri] : undefined,
      [uri]
    )
  );
}

export function useTopic<
  Package extends string & keyof Messages,
  Message extends string & keyof Messages[Package]
>(uri?: string, topic?: string, _?: `${Package}/${Message}`) {
  const subscribe = useStore(useCallback(state => state.subscribe, []));
  const unsubscribe = useStore(useCallback(state => state.unsubscribe, []));

  useEffect(
    () => {
      if (uri && topic) {
        subscribe(uri, topic);

        return () => unsubscribe(uri, topic);
      }
    },
    [subscribe, unsubscribe, uri, topic]
  );


  const connectionTopic = useStore(useCallback(state => uri && topic ? state.connections[uri]?.topics[topic]: undefined, [uri, topic]));
  if (typeof connectionTopic === 'number' || typeof connectionTopic === 'undefined') {
    return undefined;
  } else {
    return connectionTopic.value as Messages[Package][Message] | undefined;
  }
}