import geometry_msgs from "./geometry_msgs";
import std_msgs from "./std_msgs";

type mesh_msgs = {
  MeshTriangleIndices: {
    vertex_indices: [number, number, number]
  }

  MeshGeometry: {
    vertices: geometry_msgs['Vector3'][]
    vertex_normals: geometry_msgs['Vector3'][]
    faces: mesh_msgs['MeshTriangleIndices'][]
  }

  MeshGeometryStamped: {
    header: std_msgs['Header']
    uuid: string
    mesh_geometry: mesh_msgs['MeshGeometry']
  }
}

export default mesh_msgs;
