import geometry_msgs from './geometry_msgs';

enum TF2Errors {
    NO_ERROR = 0,
    LOOKUP_ERROR = 1,
    CONNECTIVITY_ERROR = 2,
    EXTRAPOLATION_ERROR = 3,
    INVALID_ARGUMENT_ERROR = 4,
    TIMEOUT_ERROR = 5,
    TRANSFORM_ERROR = 6,
}

type tf2_msgs = {
  TFMessage : {
    transforms: geometry_msgs['TransformStamped'][]
  }

  TF2Error : {
    error : TF2Errors,
    error_string : string
  }
};

export default tf2_msgs;
