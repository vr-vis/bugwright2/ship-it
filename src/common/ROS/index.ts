import std_msgs from './std_msgs';
import geometry_msgs from './geometry_msgs';
import sensor_msgs from './sensor_msgs';
import mesh_msgs from './mesh_msgs';
import shape_msgs from './shape_msgs';
import tf2_msgs from './tf2_msgs'

export type Messages = {
  std_msgs: std_msgs;
  geometry_msgs: geometry_msgs;
  sensor_msgs: sensor_msgs;
  shape_msgs: shape_msgs;
  mesh_msgs: mesh_msgs;
  tf2_msgs: tf2_msgs;
}
