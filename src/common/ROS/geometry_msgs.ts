import std_msgs from './std_msgs';

type geometry_msgs = {
  Vector3: {
    x: number
    y: number
    z: number
  }

  Point: {
    x: number
    y: number
    z: number
  }

  Quaternion: {
    x: number
    y: number
    z: number
    w: number
  }

  Transform: {
    translation: geometry_msgs['Vector3']
    rotation: geometry_msgs['Quaternion']
  }

  TransformStamped: {
    header: std_msgs['Header']
    child_frame_id: string
    transform: geometry_msgs['Transform']
  }

  Pose: {
    position: geometry_msgs['Point']
    orientation: geometry_msgs['Quaternion']
  }

  PoseStamped: {
    header: std_msgs['Header']
    pose: geometry_msgs['Pose']
  }

  PoseArray: {
    header: std_msgs['Header']
    poses: geometry_msgs['Pose'][]
  }
};

export default geometry_msgs;
