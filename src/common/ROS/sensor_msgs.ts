import std_msgs from "./std_msgs";

enum PowerSupplyStatus {
  Unknown = 0,
  Charging = 1,
  Discharging = 2,
  NotCharging = 3,
  Full = 4,
}

enum PowerSupplyHealth {
  Unknown = 0,
  Good = 1,
  Overheat = 2,
  Dead = 3,
  Overvoltage = 4,
  UnspecFailure = 5,
  Cold = 6,
  WatchdogTimerExpire = 7,
  SafetyTimerExpire = 8,
}

enum PowerSupplyTechnology {
  Unknown = 0,
  NiMH = 1,
  Lion = 2,
  LiPo = 3,
  LiFE = 4,
  NiCd = 5,
  LiMn = 6,
}

enum PointFieldDataType {
  Int8 = 1,
  UInt8 = 2,
  Int16 = 3,
  UInt16 = 4,
  Int32 = 5,
  UInt32 = 6,
  Float32 = 7,
  Float64 = 8,
}

type sensor_msgs = {
  BatteryState: {
    header: std_msgs['Header']
    voltage: number
    temperature: number
    current: number
    charge: number
    capacity: number
    design_capacity: number
    percentage: number

    power_supply_status: PowerSupplyStatus
    power_supply_health: PowerSupplyHealth
    power_supply_technology: PowerSupplyTechnology
    present: boolean

    cell_voltage: number[]
    cell_temperature: number[]

    location: string
    serial_number: string
  }

  PointField: {
    name: string
    offset: number
    datatype: PointFieldDataType
    count: number
  }

  PointCloud2: {
    width: number
    height: number
    fields: sensor_msgs['PointField'][]
    is_bigendian: boolean
    point_step: number
    row_step: number
    data: string
    is_dense: boolean
  }

  CompressedImage: {
    header: std_msgs['Header']
    format: 'jpeg' | 'png'
    data: string
  }

  Image: {
    width: number
    height: number
    encoding: 'rgb8' | 'mono8' | '16UC1' | 'bgr8' | string // There are more, but these are the supported ones
    step: number
    data: string
  }
}

export default sensor_msgs;
