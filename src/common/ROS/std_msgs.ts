type std_msgs = {
  Header: {
    seq: number;
    time: {
      sec: number;
      nsec: number;
    };
    frame_id: string;
  };
}

export default std_msgs;
