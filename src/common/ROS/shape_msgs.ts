import geometry_msgs from "./geometry_msgs";

type shape_msgs = {
  MeshTriangle: {
    vertex_indices: [number, number, number]
  }

  Mesh: {
    triangles: shape_msgs['MeshTriangle'][]
    vertices: geometry_msgs['Point'][]
  }
}

export default shape_msgs;
