import { useCallback, useEffect, useState } from "react";
import { Service as ROSService } from "roslib";
import { Services } from "./BugWright2";
import { useConnection } from "./store";

export interface Service {
  Request: any;
  Response: any;
}

// Package extends string & keyof Messages,
export function useService<ServiceType extends string & keyof Services>(uri?: string, name?: string, serviceType?: ServiceType): (request: Services[ServiceType]['Request']) => Promise<Services[ServiceType]['Response']> {
  const connection = useConnection(uri);

  const [service, setService] = useState<ROSService>();

  useEffect(
    () => {
      if (connection?.rosbridge && name && serviceType) {
        setService(new ROSService<Services[ServiceType]['Request'], Services[ServiceType]['Response']>({
          ros: connection?.rosbridge,
          name, serviceType,
        }));
        return () => setService(undefined);
      }
    },
    [connection?.rosbridge, name, serviceType]
  );

  const callback = useCallback((request: Services[ServiceType]['Request']) => {
    return new Promise<Services[ServiceType]['Response']>((resolve, reject) => {
      service?.callService(request, (response) => resolve(response), reject);
    });
  }, [service]);

  return callback;
}
