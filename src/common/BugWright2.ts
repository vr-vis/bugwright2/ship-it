import { Messages } from "./ROS"
import geometry_msgs from "./ROS/geometry_msgs";
import mesh_msgs from "./ROS/mesh_msgs";
import { Service } from "./service";

export enum MissionType {
  INSPECT_POINT = 0,
  INSPECT_PATH = 1,
  INSPECT_AREA = 2,
}

export type Mission = {
  mission_type: MissionType,
  points: geometry_msgs['Point'][];
}

export type Services = {
  PrepareMission: {
    Request: {
      missions: Mission[]
    },
    Response: {
      id: number
      planned_paths: geometry_msgs['PoseArray'][]
      mission_reference: number[]
    },
  },
  ExecuteMission: {
    Request: {
      id: number
    },
    Response: {
      response: boolean
    },
  },
  GetGeometry: {
    Request: {
      uuid: string
    },
    Response: {
      mesh_geometry_stamped: mesh_msgs['MeshGeometryStamped']
    }
  }
}
