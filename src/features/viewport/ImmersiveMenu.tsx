import { Text } from "@react-three/drei";
import { useFrame } from "@react-three/fiber";
import { Interactive, useController, useXR, useXREvent, XRController } from "@react-three/xr";
import { useCallback, useEffect, useLayoutEffect, useRef, useState } from "react";
import { ColorRepresentation, Matrix4, Quaternion, Texture, Vector3, XRHandSpace } from "three";
import { useStore, useTopic } from "../../common/store";
import { Robot } from "../../schemas/Robot.schema";

const width = 0.6;
const lineHeight = 0.12;
const spacing = 0.008;

function Line(props: { start: Vector3, end: Vector3, color?: ColorRepresentation }) {
  const ref = useRef<any>();

  useLayoutEffect(() => {
    ref.current.geometry.setFromPoints([props.start, props.end]);
  }, [props.start, props.end])

  return (
    <line ref={ref}>
      <bufferGeometry />
      <lineBasicMaterial color={props.color || 'black'} />
    </line>
  )
}

function Lines(props: { points: Vector3[], color?: ColorRepresentation }) {
  const ref = useRef<any>();

  useLayoutEffect(() => {
    ref.current.geometry.setFromPoints(props.points);
  }, [props.points])

  return (
    <line ref={ref}>
      <bufferGeometry />
      <lineBasicMaterial color={props.color || 'black'} />
    </line>
  )
}

export interface RobotListItemProps {
  config: Robot;
  selected?: boolean;
  position: number;
  onStartControlling: (robot: string) => void;
}
const RobotListItem = (props: RobotListItemProps ) => {
  const [activeImageStream, setActiveImageStream] = useState(0);
  const [image] = useState(new Image());
  const [texture] = useState(new Texture(image));
  const compressedImage = useTopic(props.config.rosbridge.uri, props.config.topics.images?.at(activeImageStream)?.topic, 'sensor_msgs/CompressedImage');

  useEffect(() => {
    image.onload = () => {
      texture.needsUpdate = true;
    }
  }, [image, texture]);

  useEffect(() => {
    if (compressedImage) {
      image.src = `data:image/${compressedImage.format};base64, ${compressedImage.data}`;
    }
  }, [compressedImage, image]);

  return (
    <group position={[0, props.position, 0]}>
      <Interactive
        onSelect={() => props.onStartControlling(props.config.name)}
      >
        <mesh>
          <planeGeometry args={[width, lineHeight]} />
          <meshStandardMaterial color={[0, 0, 0]} opacity={0.8} transparent />
        </mesh>
        {
          props.selected ?
            <Lines points={[
              new Vector3(-width / 2, lineHeight / 2, 0.01),
              new Vector3(width / 2, lineHeight / 2, 0.01),
              new Vector3(width / 2, -lineHeight / 2, 0.01),
              new Vector3(-width / 2, -lineHeight / 2, 0.01),
              new Vector3(-width / 2, lineHeight / 2, 0.01),
            ]} color="green" />
          :
          null
        }
        <mesh position={[-width / 2 + lineHeight /2, 0, 0.01]}>
          <planeGeometry args={[lineHeight, lineHeight]} />
          <meshStandardMaterial color={[1, 1, 1]} map={texture} />
        </mesh>
        <Text position={[0, 0, 0.01]} lineHeight={lineHeight}>
          { props.config.name }
        </Text>
      </Interactive>
    </group>
  );
}

export interface ImmersiveMenuProps {
  selectedRobot?: string;
  selectRobot: (robot: string) => void;
  onStartControlling: (robot: string) => void;
}

const ImmersiveMenu = (props: ImmersiveMenuProps) => {
  const { session } = useXR();
  const [controller, setController] = useState<XRController>();
  const [controllerPosition, setControllerPosition] = useState(new Vector3());
  const [controllerQuaternion, setControllerQuaternion] = useState(new Quaternion());
  const robots = useStore(state => state.robots);
  const [stickPosition, setStickPosition] = useState<"middle" | "up" | "down">("middle");

  const selectNextRobot = () => {
      const robotNames = Object.keys(robots);
      const selectedRobotIndex = robotNames.findIndex(robot => robot === props.selectedRobot);
      props.selectRobot(robotNames[(selectedRobotIndex + 1) % robotNames.length]);
  };

  const selectPreviousRobot = () => {
      const robotNames = Object.keys(robots);
      const selectedRobotIndex = robotNames.findIndex(robot => robot === props.selectedRobot);
      if (selectedRobotIndex <= 0) {
        // first or none selected
        props.selectRobot(robotNames[robotNames.length - 1]);
      } else {
        props.selectRobot(robotNames[selectedRobotIndex - 1]);
      }
  };

  useFrame(() => {
    if (session && controller) {
      setControllerPosition(controller.controller.getWorldPosition(new Vector3()));
      setControllerQuaternion(controller.controller.getWorldQuaternion(new Quaternion()));

      const gamepad = controller.inputSource.gamepad;
      if (gamepad) {
        const axis = 3;
        const axisValue = gamepad.axes[axis];
        if (axisValue > 0.9) {
          if (stickPosition !== "up") {
            setStickPosition("up");
            selectPreviousRobot();
          }
        } else if (axisValue < -0.9) {
          if (stickPosition !== "down") {
            setStickPosition("down");
            selectNextRobot();
          }
        } else if (axisValue > -0.1 && axisValue < 0.1) {
          if (stickPosition !== "middle") {
            setStickPosition("middle");
          }
        }
      }
    }
  });

  useXREvent("squeezestart", event => { if (!controller) { setController(event.target) } });
  useXREvent("squeezeend", event => { if (controller === event.target) { setController(undefined); } });

  if (!controller) {
    return <></>;
  }


  // const totalHeight = Object.keys(robots).length * lineHeight;

  return (
    <group position={controllerPosition} quaternion={controllerQuaternion}>
      <group position={[0, 0.2, 0]}>
        {
          Object.values(robots).map((robotConfig, i) =>
            <RobotListItem
              position={i * lineHeight + i * spacing}
              config={robotConfig}
              selected={props.selectedRobot === robotConfig.name}
              onStartControlling={props.onStartControlling}
            />
          )
        }
      </group>
    </group>
  );
}

export default ImmersiveMenu
