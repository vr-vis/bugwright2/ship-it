import { Text } from '@react-three/drei';
import { Interactive } from '@react-three/xr';
import { PropsWithoutRef, useEffect, useState } from 'react';
import { Euler, Quaternion, Vector3 } from 'three';
import { useTopic } from '../../common/store';
import { Robot as RobotConfig } from '../../schemas/Robot.schema';
import RobotDetails from './RobotDetails';

export interface ImmersiveRobotProps {
  config: RobotConfig;
  onSelect: () => void;
  onStartControlling: () => void;
  selected?: boolean;
}

export default function ImmersiveRobot(props: PropsWithoutRef<ImmersiveRobotProps>) {
  const [hovering, setHovering] = useState(false);
  const poseStamped = useTopic(props.config.rosbridge.uri, props.config.topics.pose, 'geometry_msgs/PoseStamped');

  let position = new Vector3();
  let orientation = new Quaternion();

  // If we haven't received a pose yet: do not render the robot.
  if (poseStamped) {
    position.set(-poseStamped.pose.position.x, poseStamped.pose.position.z, poseStamped.pose.position.y);
    orientation.set(-poseStamped.pose.orientation.x, poseStamped.pose.orientation.z, poseStamped.pose.orientation.y, poseStamped.pose.orientation.w);
    orientation.multiply(new Quaternion().setFromEuler(new Euler(0, -Math.PI / 2, Math.PI), true));
  }

  return (
    <group
      position={position}
    >
      {
        props.selected ?
          <group position={[0, 2.25, 0]}>
            <RobotDetails config={props.config} />
          </group>
          :
          null
      }
      <Interactive
        onHover={() => setHovering(true)}
        onBlur={() => setHovering(false)}
        onSelect={() => props.selected ? props.onStartControlling() : props.onSelect()}
      >
        <mesh quaternion={orientation}>
          <sphereGeometry />
          {
            props.selected ?
              hovering ?
                <meshPhongMaterial color="blue" />
                :
                <meshPhongMaterial color="green" />
              :
              hovering ?
                <meshPhongMaterial color="green" />
                :
                <meshPhongMaterial color="red" />
          }
        </mesh>
      </Interactive>
    </group>
  );
}

