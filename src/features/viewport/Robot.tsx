import { PropsWithoutRef, useEffect, useMemo, useRef, useState } from 'react';
import { Euler, Group, MathUtils, Object3D, Quaternion, Vector3 } from 'three';
import { useTopic } from '../../common/store';
import { Robot as RobotConfig } from '../../schemas/Robot.schema';
import { Frame } from './TF2';
import { useLoader } from '@react-three/fiber'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';


export interface RobotProps {
  config: RobotConfig;
}

export default function Robot(props: PropsWithoutRef<RobotProps>) {
  const [hovering, setHovering] = useState(false);
  const poseStamped = useTopic(props.config.rosbridge.uri, props.config.topics.pose, 'geometry_msgs/PoseStamped');
  const gltf = useLoader(GLTFLoader, props.config.mesh?.uri || "");
  const modelClone = useMemo<Object3D>(() => { return gltf.scene.clone(true); }, [gltf]);

  const static_quaternion = new Quaternion();
  if (props.config.mesh?.transform?.rotation) {
    static_quaternion.setFromEuler(new Euler(MathUtils.degToRad(props.config.mesh.transform.rotation[0]), MathUtils.degToRad(props.config.mesh.transform.rotation[1]), MathUtils.degToRad(props.config.mesh.transform.rotation[2]), "XYZ"), true);
  }

  return (
    <>
      {
        props.config.topics.pose != "" && poseStamped ?

          // Pose based
          <group
            position={[-poseStamped.pose.position.x, poseStamped.pose.position.z, poseStamped.pose.position.y]}
            quaternion={[-poseStamped.pose.orientation.x, poseStamped.pose.orientation.z, poseStamped.pose.orientation.y, poseStamped.pose.orientation.w]}
            scale={[1, 1, 1]}
          >
            <group
              position={props.config.mesh?.transform?.translation}
              quaternion={static_quaternion}
              scale={props.config.mesh?.transform?.scale}
            >
              <primitive object={modelClone} />
            </group>
          </group>
          :

          // tf-Frame
          <Frame frameId={props.config.poseFrame}>
            <group
              position={props.config.mesh?.transform?.translation}
              quaternion={static_quaternion}
              scale={props.config.mesh?.transform?.scale}
            >
              <primitive object={modelClone} />
            </group>
          </Frame>

      }
    </>
  );
}

