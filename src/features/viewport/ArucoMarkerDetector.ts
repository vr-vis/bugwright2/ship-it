import { Matrix4, Vector3, Quaternion, Euler, MathUtils } from 'three';
declare var cv: any; // OpenCV defines
declare var knownCameraCalibrations: {[key:string] : CameraCalibration}; // Camera Calibrations

type CameraCalibration = {
    "camera": {
        "res_x": number,
        "res_y": number,
        "f_x": number,
        "f_y": number,
        "c_x": number,
        "c_y": number,
    },
    "distortion": {
        "k_1": number,
        "k_2": number,
        "k_3": number,
        "k_4": number,
        "k_5": number,
        "k_6": number,
        "p_1": number,
        "p_2": number
    },
    "offset":{
        "x": number,
        "y": number,
        "z": number,
        "r_x": number,
        "r_y": number,
        "r_z": number
    }
};

class ArcuoMarkerDetector {
    src: any;
    dst: any;
    detectorParams: any;
    dict: any;
    cameraMatrix: any;
    distCoeffs: any;
    OpenCV: any;
    MarkerSize: number;
    OffsetTransform: Matrix4 = new Matrix4().identity();
    OutputCallback: (detectedMarkers: { [key: string]: number[] }, camera: number[]) => void;

    constructor(OpenCV: any, Width: number, Height: number, CameraCalibration: CameraCalibration, MarkerSize: number, OutputCallback: (detectedMarkers: { [key: string]: number[] }, camera: number[]) => void) {
        this.OpenCV = OpenCV;

        this.src = new OpenCV.Mat(Height, Width, OpenCV.CV_8UC4);
        this.dst = new OpenCV.Mat(Height, Width, OpenCV.CV_8UC1);

        this.detectorParams = new OpenCV.aruco_DetectorParameters();
        this.dict = new OpenCV.aruco_Dictionary(OpenCV.DICT_5X5_250);
        this.MarkerSize = MarkerSize;

        const xScale = Width / CameraCalibration.camera.res_x;
        const yScale = Height / CameraCalibration.camera.res_y;
        this.cameraMatrix = OpenCV.matFromArray(3, 3, OpenCV.CV_64F, [CameraCalibration.camera.f_x * xScale, 0, CameraCalibration.camera.c_x * xScale, 0, CameraCalibration.camera.f_y * yScale, CameraCalibration.camera.c_y * yScale, 0, 0, 1]);
        this.distCoeffs = OpenCV.matFromArray(14, 1, OpenCV.CV_64F, [CameraCalibration.distortion.k_1, CameraCalibration.distortion.k_2, CameraCalibration.distortion.p_1, CameraCalibration.distortion.p_2, CameraCalibration.distortion.k_3, CameraCalibration.distortion.k_4, CameraCalibration.distortion.k_5, CameraCalibration.distortion.k_6]);
        this.OffsetTransform.compose(new Vector3(CameraCalibration.offset.x, CameraCalibration.offset.y, CameraCalibration.offset.z), new Quaternion().setFromEuler(new Euler(MathUtils.degToRad(CameraCalibration.offset.r_x), MathUtils.degToRad(CameraCalibration.offset.r_y), MathUtils.degToRad(CameraCalibration.offset.r_z))), new Vector3(1,1,1));

        this.OutputCallback = OutputCallback;

        // Change Detector Parameters here
        this.detectorParams.useAruco3Detection = true;
        this.detectorParams.cornerRefinementMethod = OpenCV.CORNER_REFINE_SUBPIX;
    }

    destruct() {
        this.src.delete(); this.dst.delete(); this.detectorParams.delete(); this.dict.delete(); this.cameraMatrix.delete(); this.distCoeffs.delete();
    }

    // Rodrigues to Quaternion
    static rvec2quaternion(rvec: [number, number, number]): [number, number, number, number] {
        let angle = Math.sqrt(rvec[0] * rvec[0] + rvec[1] * rvec[1] + rvec[2] * rvec[2]);
        let axis = [rvec[0] / angle, rvec[1] / angle, rvec[2] / angle];
        let sin_half_angle = Math.sin(angle / 2);

        return [axis[0] * sin_half_angle, axis[1] * sin_half_angle, axis[2] * sin_half_angle, Math.cos(angle / 2)];
    }

    //Remap axis from OpenCV aruco system to threejs system
    static remapFromMarkerSystem(Pos: [number, number, number], Rot: [number, number, number, number]): [Vector3, Quaternion] {
        const RemappedPos = new Vector3(Pos[0], -Pos[1], -Pos[2]);
        const RemappedQuat = new Quaternion(Rot[0], -Rot[1], -Rot[2], Rot[3]);
        return [RemappedPos, RemappedQuat];
    }

    processFrame(pixels: ArrayBuffer, camera: number[]) {
        this.src.data.set(new Uint8ClampedArray(pixels));

        let markerIds = new this.OpenCV.Mat();
        let markerCorners = new this.OpenCV.MatVector();
        let detected_markers = {} as { [key: string]: number[] };

        this.OpenCV.cvtColor(this.src, this.dst, this.OpenCV.COLOR_RGBA2GRAY);

        this.OpenCV.detectMarkers(this.dst, this.dict, markerCorners, markerIds, this.detectorParams);

        if (markerCorners.size() > 0) {
            let rvecs = new this.OpenCV.Mat();
            let tvecs = new this.OpenCV.Mat();

            this.OpenCV.estimatePoseSingleMarkers(markerCorners, this.MarkerSize, this.cameraMatrix, this.distCoeffs, rvecs, tvecs);

            let temp_rotMat = new this.OpenCV.Mat();
            for (let i = 0; i < markerIds.total(); i++) {
                let single_rot = ArcuoMarkerDetector.rvec2quaternion(rvecs.row(i).data64F as [number, number, number]);
                let single_pos = tvecs.row(i).data64F as [number, number, number];

                let [RemappedPos, RemappedQuat] = ArcuoMarkerDetector.remapFromMarkerSystem(single_pos, single_rot);

                let matrix = new Matrix4().compose(RemappedPos, RemappedQuat, new Vector3(1, 1, 1));
                matrix.premultiply(this.OffsetTransform);
                detected_markers["" + markerIds.data32S[i]] = matrix.toArray();
            }
            
            rvecs.delete(); tvecs.delete(); temp_rotMat.delete();
        }
        
        markerIds.delete(); markerCorners.delete();
        this.OutputCallback(detected_markers, camera);
    }
}

var workerStorage: { MarkerDetector: ArcuoMarkerDetector | null } = { MarkerDetector: null };
onmessage = function (e) {
    switch (e.data.cmd) {
        case 'load': {
            // Import Webassembly script
            importScripts('/opencv.js');
            importScripts('/KnownCameraCalibrations.js');
            cv.then((OpenCV) => {
                if (workerStorage.MarkerDetector == null) {
                    let CameraCalibration = knownCameraCalibrations["HoloLens2"];
                    workerStorage.MarkerDetector = new ArcuoMarkerDetector(OpenCV, e.data.width, e.data.height, CameraCalibration, e.data.markerSize, (detectedMarkers: { [key: string]: number[] }, camera: number[]) => {
                        this.postMessage({ "cmd": "result", "data": detectedMarkers, "camera": camera });
                    });
                }
                postMessage({ "cmd": "loaded" });
            });
            break
        }
        case 'unload': {
            if (workerStorage.MarkerDetector != null) {
                workerStorage.MarkerDetector.destruct();
            }
            postMessage({ "cmd": "unloaded" });
            break
        }
        case 'processImage':
            workerStorage.MarkerDetector?.processFrame(e.data.pixels, e.data.camera);
    }
}