import { useCubeTexture } from '@react-three/drei';
import { useThree } from '@react-three/fiber'
import { useEffect } from 'react';

export interface SkyBoxProps {
  baseURL: string;
  visible?: boolean;
}

function SkyBox(props: SkyBoxProps) {
  const { scene } = useThree();
  let texture = useCubeTexture(["east.jpeg", "west.jpeg", "up.jpeg", "down.jpeg", "north.jpeg", "south.jpeg"], { path: props.baseURL });
  useEffect(
    () => {
      if (!scene || !texture) return;
      scene.background = props.visible ? texture : null;
    },
    [scene, texture, props.visible]
  );

  return null;
}

export default SkyBox;
