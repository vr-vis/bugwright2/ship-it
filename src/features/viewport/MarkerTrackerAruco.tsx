import { useThree } from '@react-three/fiber';
import { useRef, useEffect } from 'react';
import { Object3D, Matrix4, Vector3, Mesh, BoxGeometry, Quaternion, MathUtils, MeshBasicMaterial } from 'three';
import consolere from 'console-remote-client';
import { useSocket } from '../../hooks/useSocket';


class Marker {
    knownPose = new Matrix4().identity();
    lastTransfrom = new Matrix4().identity();
    startedTracking = new Date(0);
    lastTracking = new Date(0);
    isTracking = false;
    debugVisualization: Mesh | null = null;

    // constructor(ul_point: [number, number, number], ll_point: [number, number, number], lr_point: [number, number, number]) {
    //     let base1 = new Vector3(ul_point[0] - ll_point[0], ul_point[1] - ll_point[1], ul_point[2] - ll_point[2]);
    //     let base2 = new Vector3(lr_point[0] - ll_point[0], lr_point[1] - ll_point[1], lr_point[2] - ll_point[2]);
    //     let center_pos = new Vector3().fromArray(ll_point).addScaledVector(base1, 0.5).addScaledVector(base2, 0.5);
    //     base1.normalize(); base2.normalize();
    //     let quat = new Quaternion().setFromRotationMatrix(new Matrix4().makeBasis(base2, base1, base2.clone().cross(base1)));

    //     this.knownPose = new Matrix4().compose(center_pos, quat, new Vector3(1, 1, 1));
    // }

    constructor(knownPose : Matrix4){
        this.knownPose = knownPose;
    }
}


interface ArcuoMarkerDetectorProps {
    // knownMarkers: { [key: string]: [[number, number, number], [number, number, number], [number, number, number]] };
    markerSize: number; //0.10917
}

function MarkerTrackerAruco(props: ArcuoMarkerDetectorProps) {
    const videoElement = useRef<HTMLVideoElement | null>(null);
    const resizedVideoCanvas = useRef<HTMLCanvasElement | null>(null);
    const videoProcesser = useRef<Worker | null>(null);
    const interval = useRef<NodeJS.Timer | null>(null);
    const knownMarkers = useRef<{ [key: string]: Marker }>({});

    // ThreeJS Stuff
    const { gl, camera: virtualCamera, scene } = useThree();
    const worldRefSpace = useRef<XRReferenceSpace | null>(gl.xr.getReferenceSpace());
    const setOnce = useRef<boolean>(false);
    const deviceCamera = useRef<Object3D | null>(null);
    
    useEffect(() => {
        consolere.connect({
            server: 'https://console.re', // optional, default: https://console.re
            channel: 'BuggyWright', // required
            redirectDefaultConsoleToRemote: true, // optional, default: false
            disableDefaultConsoleOutput: false, // optional, default: false

        });
        setTimeout(() => { console.log("This is remote"); }, 2000);
    }, []);

    useEffect(() => {
        videoElement.current = document.createElement('video');
        videoElement.current.width = 1920;
        videoElement.current.height = 1080;
        document.body.appendChild(videoElement.current);

        return () => {
            if (videoElement.current != null) {
                document.body.removeChild(videoElement.current);
            }
        };
    }, []);

    useSocket("192.168.137.1:1234", (e) => {
        console.log(e.data);
        let value = parseFloat((e.data as string).substring(1));
        if (deviceCamera.current == null) return;
        switch ((e.data as string)[0]) {
            case "X":
                deviceCamera.current.translateX(value);
                console.log("X:" + value);
                break;
            case "Y":
                deviceCamera.current.translateY(value);
                console.log("Y:" + value);
                break;
            case "Z":
                deviceCamera.current.translateZ(value);
                console.log("Z:" + value);
                break;
            case "P":
                deviceCamera.current.rotateX(MathUtils.degToRad(value));
                console.log("RX:" + value);
                break;
            case "Y":
                deviceCamera.current.rotateY(MathUtils.degToRad(value));
                console.log("RY:" + value);
                break;
            case "R":
                deviceCamera.current.rotateZ(MathUtils.degToRad(value));
                console.log("RZ:" + value);
                break;
            case "S":
                deviceCamera.current.scale.addScalar(value);
                console.log("S:" + value);
                break;
            case "?":
                if (deviceCamera.current) {
                    console.log("Pos: " + deviceCamera.current.position.x + "|" + deviceCamera.current.position.y + "|" + deviceCamera.current.position.z);
                    console.log("Rot: " + deviceCamera.current.rotation.x + "|" + deviceCamera.current.rotation.y + "|" + deviceCamera.current.rotation.z);
                    console.log("Scale: " + deviceCamera.current.scale.x);
                }
                break;
        }
    });

    // Attach camera proxy
    useEffect(() => {
        if (virtualCamera == undefined) return;
        
        if (deviceCamera.current == null) {
            deviceCamera.current = new Object3D();
            virtualCamera.add(deviceCamera.current);
        }
        
        return () => {
            deviceCamera.current?.removeFromParent();
        }
    }, [virtualCamera]);

    //Dynamic anchor registration
    useEffect(()=>{
        let knownMarkersTimer = setInterval(()=> {
            if(knownMarkers.current){
                registerAnchors(scene, knownMarkers.current);
            }
        }, 5000);

        return () => {
            clearInterval(knownMarkersTimer);
            if(knownMarkers.current){
                for (let [key, value] of Object.entries(knownMarkers.current)) {
                    value.debugVisualization?.removeFromParent();
                }
            }
            knownMarkers.current = {};
        };
    }, [scene, knownMarkers]);

    function registerAnchors(root : Object3D, alreadyKnownMarkers: {[key: string]: Marker}) : void {
        const prefix = "fiducial_";
        if ( root.name.startsWith(prefix)){
            const markerNumber = root.name.substring(prefix.length);
            if(!(markerNumber in alreadyKnownMarkers)){
                const geometry = new BoxGeometry(props.markerSize - 0.01, props.markerSize - 0.01, 0.01);
                const material = new MeshBasicMaterial({ color: 'hsl(' + (parseInt(markerNumber) - 99) / 15 * 180.0 + ', 100%, 50%)' });
                const cube = new Mesh(geometry, material);
                cube.matrixAutoUpdate = false;
    
                alreadyKnownMarkers[markerNumber] = new Marker(root.matrixWorld);
                alreadyKnownMarkers[markerNumber].debugVisualization = cube;
                scene.add(cube);
            }
        }
    
        for ( let i = 0, l = root.children.length; i < l; i ++ ) {
            registerAnchors(root.children[i], alreadyKnownMarkers);
        }
    }

    // Take image from current webcam footage and send to worker
    function WorkerSendImageData() {
        if (videoProcesser.current == null || resizedVideoCanvas.current == null || videoElement.current == null) return;

        const context = resizedVideoCanvas.current?.getContext("2d");
        if (context != null) {
            context.drawImage(videoElement.current, 0, 0, resizedVideoCanvas.current.width, resizedVideoCanvas.current.height);
            const data = context.getImageData(0, 0, resizedVideoCanvas.current.width, resizedVideoCanvas.current.height).data;
            videoProcesser.current.postMessage({ "cmd": "processImage", "pixels": data.buffer, "camera": deviceCamera.current?.matrixWorld.toArray() }, [data.buffer]);
        }
    }

    // Process response from worker
    function WorkerMessage(event: any) {
        switch (event.data.cmd) {
            case 'result': {
                MarkersDetected(event.data.data, event.data.camera);
                WorkerSendImageData();
                break;
            }
            case 'loaded': { //start the loop
                WorkerSendImageData();
                break
            }
            case 'unloaded':
                videoProcesser.current = null;
                break;
        }
    }

    // Callback from Aruco Marker Detector
    function MarkersDetected(detectedMarkers: { [key: string]: number[] }, cameraMatrixWorld: number[]) {
        if (knownMarkers.current == undefined) {
            console.log("Known markers undefined");
            return;
        }

        // Update knownMarkers
        for (let [key, value] of Object.entries(knownMarkers.current)) {
            if (detectedMarkers.hasOwnProperty(key)) {
                //Was not tracking
                if (!value.isTracking) {
                    value.isTracking = true;
                    // just lost for a second
                    if (Math.floor((Date.now() - value.lastTracking.getTime())) < 1000) {
                        console.log(key + " tracking again. Was lost for a few frames.");
                    } else {
                        value.startedTracking = new Date();
                        console.log(key + " now tracking");
                    }
                }
                value.lastTransfrom = new Matrix4().fromArray(detectedMarkers[key]);
                value.lastTracking = new Date();

                if (value.debugVisualization != null) {
                    value.debugVisualization.visible = true;
                }
            } else {
                value.isTracking = false;
                if (value.debugVisualization != null) {
                    value.debugVisualization.visible = false;
                }
            }
        }

        for (let [key, value] of Object.entries(knownMarkers.current)) {
            if (!value.isTracking) continue;


            if (value.debugVisualization != null && deviceCamera.current != null) {
                value.debugVisualization.matrix = new Matrix4().fromArray(cameraMatrixWorld).clone().multiply(value.lastTransfrom.clone());
            }

            if (Math.floor((Date.now() - value.startedTracking.getTime())) < 6000) {
                console.log("Tracking, but not long enough.")
                continue;
            }

            const knownPos = value.knownPose.clone();
            const trackedPosInverse = value.lastTransfrom.clone().invert();
            const CamPosInverse = new Matrix4().fromArray(cameraMatrixWorld).invert();
            const Offset = knownPos.multiply(trackedPosInverse).multiply(CamPosInverse).invert();

            var translation = new Vector3();
            var rotation = new Quaternion();
            Offset.decompose(translation, rotation, new Vector3());

            if (worldRefSpace.current == null) {
                worldRefSpace.current = gl.xr.getReferenceSpace();
            }

            if (worldRefSpace.current != null && !setOnce.current) {
                var OffsetSpace = worldRefSpace.current.getOffsetReferenceSpace(new XRRigidTransform(
                    { x: translation.x, y: translation.y, z: translation.z },
                    { x: rotation.x, y: rotation.y, z: rotation.z, w: rotation.w }
                ));
                gl.xr.setReferenceSpace(OffsetSpace);
                setOnce.current = true;
            }
        }

    }

    // Start Video Processing
    useEffect(() => {
        if (videoElement.current !== null && videoElement.current.srcObject == null) {
            if(!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia){
                console.error("getUserMedia not available. Did you launch in HTTPS?");
                return;
            }
            navigator.mediaDevices.getUserMedia({
                audio: false,
                video: {
                    width: { min: 640, ideal: videoElement.current.width, max: 1920 },
                    height: { min: 360, ideal: videoElement.current.height, max: 1080 }
                }
            }).then((stream) => {
                if (videoElement.current === null) return;

                videoElement.current.srcObject = stream;
                videoElement.current.play();

                resizedVideoCanvas.current = document.createElement('canvas');
                resizedVideoCanvas.current.width = 1920;
                resizedVideoCanvas.current.height = 1080;

                videoProcesser.current = new Worker(new URL("./ArucoMarkerDetector.ts", import.meta.url));
                videoProcesser.current.addEventListener('message', (evt) => WorkerMessage(evt));
                videoProcesser.current?.postMessage({ "cmd": "load", "width": resizedVideoCanvas.current.width, "height": resizedVideoCanvas.current.height, "markerSize": props.markerSize });
            }).catch(function (err) {
                console.log("An error occurred while requesting the webcam: " + err);
            });
        }

        return () => {
            if (interval.current != null) {
                clearInterval(interval.current);
            }
            videoProcesser.current?.postMessage({ "cmd": "unload" });
        };
    }, [videoElement]);

    return (
        null
    );
}

export default MarkerTrackerAruco;
