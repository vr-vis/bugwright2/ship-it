import { PerspectiveCamera, OrbitControls, Box, Plane } from '@react-three/drei';
import { Canvas, ThreeEvent } from '@react-three/fiber';
import { MeshBasicMaterial, MeshLambertMaterial } from 'three';
import For from '../../common/For';
import { useStore } from '../../common/store';
import Skybox from './SkyBox';
import Water from './Water';
import Robot from './Robot';
import TF2, { Frame } from './TF2';
import FiducialVisualizer from './FiducialVisualizer';
import { PropsWithChildren } from 'react';
import ShipModel from '../ship/ShipModel';

const Viewport = (props: PropsWithChildren<{ onMeshClick?: (event: ThreeEvent<MouseEvent>) => void }>) => {
  const robots = useStore(state => state.robots);
  const showSkybox = useStore(state => state.appSettings.showSkybox);
  const showOcean = useStore(state => state.appSettings.showSea);
  const oceanLevelOffest = useStore(state => state.appSettings.oceanLevelOffset);

  return (
    <Canvas
      tabIndex={0}
      style={{
      }}
    >
      <OrbitControls />
      <PerspectiveCamera
        position={[0, 5, -10]}
        rotation={[0, Math.PI, 0]}
        makeDefault
      />
      <ambientLight />
      <pointLight />
      <directionalLight />
      <Skybox baseURL="skyboxes/clouds/" visible={showSkybox} />
      <Water waterNormalsTexture='waternormals.jpg' size={1000} heightOffset={oceanLevelOffest} visible={showOcean} />

      <ShipModel onClick={props.onMeshClick} />

      <For each={Object.values(robots)}>
        {
          robot =>
            <Robot
              config={robot}
              key={robot.name}
            />
        }
      </For>
      <FiducialVisualizer knownMarkers={[103, 104, 105, 109]} image_uri_prefix="markers/aruco_DICT_5X5_250_Marker_" image_uri_postfix=".png" marker_size={[0.52, 0.52]} frame_prefix="fiducial_static_" />
      {/* <FiducialVisualizer knownMarkers={[103, 104, 105, 109]} image_uri_prefix="/markers/aruco_DICT_5X5_250_Marker_" image_uri_postfix=".png" marker_size={[0.52, 0.52]} frame_prefix="/mussol/fiducial_" color_tint='orange' /> */}
      <TF2 yaml={["meshes/fiducial_tags.yaml"]} attachPrintGraphFunction />

      {props.children}

    </Canvas>
  );
}

export default Viewport;
