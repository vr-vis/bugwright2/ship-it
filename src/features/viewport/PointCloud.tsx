import { useEffect, useState } from "react";
import { BufferGeometry, Float32BufferAttribute } from "three";
import { useTopic } from "../../common/store";

export interface PointCloud2Props {
  rosbridgeURI: string;
  topic: string;
}

const PointCloud2 = (props: PointCloud2Props) => {
  const cloud = useTopic(props.rosbridgeURI, props.topic, 'sensor_msgs/PointCloud2');
  const [geometry, setGeometry] = useState<BufferGeometry|undefined>();
  const [offset, setOffset] = useState<[number, number, number]>([0, 0, 0]);

  useEffect(() => {
    if (cloud) {
      const t0 = performance.now();
      const uint8Array = Uint8Array.from(atob(cloud.data), c => c.charCodeAt(0));
      const t1 = performance.now();
      console.log(`size: ${uint8Array.length / 1000}kb`);

      const bufferView = new DataView(uint8Array.buffer);
      const geometry = new BufferGeometry();
      const positions = new Float32Array(cloud.width * cloud.height * 3);
      const colors = new Float32Array(cloud.width * cloud.height * 3);

      const xField = cloud.fields.find(field => field.name === 'x');
      const yField = cloud.fields.find(field => field.name === 'y');
      const zField = cloud.fields.find(field => field.name === 'z');
      const colorField = cloud.fields.find(field => field.name === 'rgba');

      if (!xField || !yField || !zField || !colorField) {
        return;
      }

      console.log(`Start processing ${cloud.width * cloud.height} points`);
      const offset: [number, number, number] = [0, 0, 0];
      const t2 = performance.now();

      for (let y = 0; y < cloud.height; ++y) {
        for (let x = 0; x < cloud.width; ++x) {
          const byteOffset = cloud.row_step * y + x * cloud.point_step;
          const pointOffset = y * cloud.width + x;

          positions[pointOffset * 3 + 0] = -bufferView.getFloat32(byteOffset + xField.offset, !cloud.is_bigendian);
          positions[pointOffset * 3 + 2] = bufferView.getFloat32(byteOffset + yField.offset, !cloud.is_bigendian);
          positions[pointOffset * 3 + 1] = bufferView.getFloat32(byteOffset + zField.offset, !cloud.is_bigendian);

          const color = bufferView.getUint32(byteOffset + colorField.offset, !cloud.is_bigendian);
          colors[pointOffset * 3 + 0] = ((color >> 16) & 0xff) / 255.0;
          colors[pointOffset * 3 + 1] = ((color >> 8) & 0xff) / 255.0;
          colors[pointOffset * 3 + 2] = ((color >> 0) & 0xff) / 255.0;

          for (let i = 0; i < 3; ++i) {
            offset[i] = (pointOffset / (pointOffset + 1)) * offset[i] - positions[pointOffset * 3 + i] / (pointOffset + 1);
          }
        }
      }
      setOffset(offset);
      const t3 = performance.now();

      geometry.setAttribute('position', new Float32BufferAttribute(positions, 3));
      geometry.setAttribute('color', new Float32BufferAttribute(colors, 3));
      setGeometry(geometry);

      console.log(`Finished processing ${cloud.width * cloud.height} points`);
      console.log(`${t1 - t0} ms`);
      console.log(`${t2 - t1} ms`);
      console.log(`${t3 - t2} ms`);

      return () => geometry.dispose();
    } else {
      setGeometry(undefined);
    }
  }, [cloud]);

  return (
    <points
      geometry={geometry}
      position={offset}
    >
      <pointsMaterial size={0.01} vertexColors />
    </points>
  );
}

export default PointCloud2;
