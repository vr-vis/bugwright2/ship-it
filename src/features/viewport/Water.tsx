import { useThree } from '@react-three/fiber'
import { useEffect, useRef } from 'react';
import { TextureLoader, PlaneGeometry, RepeatWrapping, Vector3, Group, DoubleSide } from 'three'
import { Water as ThreeJSWater } from "three/examples/jsm/objects/Water";

export interface WaterProps {
  waterNormalsTexture?: string;
  size?: number;
  heightOffset?: number;
  visible?: boolean;
}

function Water(props: WaterProps) {
  const { scene } = useThree();
  let water = useRef<ThreeJSWater | null>(null);

  useEffect(() => {
    const updateInterval = setInterval(() => {
      if (water.current) water.current.material.uniforms['time'].value += 1.0 / 60.0 / 10;
    }, 1 / 60 * 1000);

    const waterGeometry = new PlaneGeometry(props.size || 100, props.size || 100, 1, 1);
    waterGeometry.computeVertexNormals();

    water.current = new ThreeJSWater(
      waterGeometry,
      {
        textureWidth: 512,
        textureHeight: 512,
        waterNormals: props.waterNormalsTexture ? new TextureLoader().load(props.waterNormalsTexture, texture => {
          texture.wrapS = texture.wrapT = RepeatWrapping;
        }) : undefined,
        sunDirection: new Vector3(),
        sunColor: 0xffffff,
        waterColor: 0x001e0f,
        distortionScale: 3.7,
        fog: scene.fog !== undefined,
        alpha: 0.1,
        side: DoubleSide,
      }
    );
    water.current.rotation.x = -Math.PI * 0.5;
    water.current.position.setY(-5);

    return () => {
      clearInterval(updateInterval);
    }
  }, [props.size, props.waterNormalsTexture]);

  return <group position={[0, props.heightOffset || 0, 0]} visible={!!props.visible}>
    {water.current ? <primitive object={water.current} /> : null}
  </group>;
}

export default Water;
