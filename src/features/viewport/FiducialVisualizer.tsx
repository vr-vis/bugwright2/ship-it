import useEnhancedEffect from "@mui/material/utils/useEnhancedEffect";
import { useTexture } from "@react-three/drei";
import { useLoader } from "@react-three/fiber";
import { useEffect } from "react";
import { BufferGeometry, DoubleSide, LinearFilter, LinearMipMapNearestFilter, MathUtils, MeshLambertMaterial, NearestFilter, Object3D, PlaneGeometry, Texture, TextureFilter } from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import For from "../../common/For";
import { Frame } from "./TF2";

export interface FiducialVisualizerSingleProps {
    frame: string;
    uri: string;
    marker_size: [number, number];
    color_tint?: string;
}

const FiducialVisualizerSingle = (props: FiducialVisualizerSingleProps) => {
    const texture = useTexture(props.uri);

    useEffect(() => {
        texture.minFilter = NearestFilter;
        texture.magFilter = NearestFilter;
    }, [texture]);

    return (
        <Frame frameId={props.frame}>
            <mesh rotation={[-Math.PI / 2, 0, Math.PI]}>
                <planeGeometry args={[props.marker_size[0], props.marker_size[1], 1, 1]} />
                <meshStandardMaterial map={texture} side={DoubleSide} color={props.color_tint} />
            </mesh>
        </Frame>
    );
}

export interface FiducialVisualizerProps {
    frame_prefix: string;
    knownMarkers: number[];
    image_uri_prefix: string;
    image_uri_postfix: string;
    marker_size: [number, number];
    color_tint?: string;
}

const FiducialVisualizer = (props: FiducialVisualizerProps) => {
    return (
        <For each={props.knownMarkers}>
            {
                marker_id =>
                    <FiducialVisualizerSingle
                        frame={(props.frame_prefix ? props.frame_prefix : "") + marker_id}
                        uri={props.image_uri_prefix + marker_id + props.image_uri_postfix}
                        marker_size={props.marker_size} color_tint={props.color_tint} />
            }
        </For>
    );
}

export default FiducialVisualizer