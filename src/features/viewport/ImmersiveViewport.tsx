import { Box } from '@react-three/drei';
import { Canvas } from '@react-three/fiber';
import { ARButton, Controllers, VRButton, XR } from '@react-three/xr';
import { useState } from 'react';
import { Group, Object3D } from 'three';
import For from '../../common/For';
import { useStore } from '../../common/store';
import FlyingControls from './FlyingControls';
import ImmersiveMenu from './ImmersiveMenu';
import ImmersiveRobot from './ImmersiveRobot';
import ImmersiveShip from './ImmersiveShip';
import MarkerTrackerAruco from './MarkerTrackerAruco';
import MeshLoader from './MeshLoader';
import SkyBox from './SkyBox';
import TF2 from './TF2';

const ImmersiveViewport = () => {
  const robots = useStore(state => state.robots);
  const [selectedRobot, selectRobot] = useState<string>();
  const [isControlling, setIsControlling] = useState(false);
  const transformtree = useStore(state => state.transformTree);

  return (
    <>
      {/* <VRButton /> */}
      <ARButton />
      <Canvas 
        tabIndex={0}
        style={{
        }}
      >
        <XR referenceSpace={"local"}>
          {/* <SkyBox baseURL="/skyboxes/clouds/" />
          <Controllers />
          <FlyingControls />
          <ImmersiveMenu selectedRobot={selectedRobot} selectRobot={selectRobot} onStartControlling={robot => { selectRobot(robot); setIsControlling(true); }} />
          <ambientLight /> */}
          <pointLight />
          {/* <ImmersiveShip selectedRobot={selectedRobot} isControlling={isControlling} finishedControlling={() => setIsControlling(false)} />
          <For each={Object.values(robots)}>
           {
           robot =>
            <ImmersiveRobot
              config={robot}
              key={robot.name}
              onSelect={() => selectRobot(robot.name)}
              selected={selectedRobot === robot.name}
              onStartControlling={() => setIsControlling(true)}
            />
           }
          </For> */}
          {/* <MeshLoader MeshURI='meshes/PortoMap.gltf' />
          <Box position={[0,1,0]}></Box>
          <mesh position={[-1.41925, 0.456493, -5.33636]}>
            <planeGeometry args={[0.52, 0.52]} />
            <meshPhongMaterial color="red" />
          </mesh> */}
          {/* <object3D name="fiducial_109"/> */}
          {/* <MarkerTrackerAruco markerSize={0.52} /> */}
          {/* <TF2 config={transformtree} debugVisualization={true}/> */}
          {/* <MarkerTrackerAruco knownMarkers={{"107": [[-1.68506, 0.178739, -5.36405],[-1.68542, 0.698728, -5.36739],[-1.16942, 0.699501, -5.30301]]}} markerSize={0.52} /> */}
        </XR>
      </Canvas>
    </>
  );
}

export default ImmersiveViewport;
