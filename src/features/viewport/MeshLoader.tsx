import { ThreeEvent, useLoader } from "@react-three/fiber";
import { useMemo } from "react";
import { BufferGeometry, DoubleSide, MathUtils, Mesh, MeshBasicMaterial, MeshLambertMaterial, Object3D, Vector3 } from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { OBB as YUKAOBB, Vector3 as YUKAVec3 } from "yuka";
import { OBB } from "three-stdlib";

export interface MeshLoaderProps {
    MeshURI: string;
    scale?: [number, number, number];
    position?: [number, number, number];
    rotation?: [number, number, number, string?];
    color?: string;
    onOBBCalculated?: (obb: OBB, parent: Object3D) => void;
    onClick?: (event: ThreeEvent<MouseEvent>) => void;
}

const MeshLoader = (props: MeshLoaderProps) => {
    const gltf = useLoader(GLTFLoader, props.MeshURI);
    const modelClone = useMemo<Object3D>(() => {
        let clonedMesh = gltf.scene.children[0].clone(true); // TODO: This is a hack, we should clone the whole scene
        let material = new MeshLambertMaterial({ color: (props.color) ? props.color : "grey", side: DoubleSide, shadowSide: DoubleSide });
        clonedMesh.traverse((object: Object3D) => {
            if (object["material"]) {
                object["material"] = material;
                if (object["geometry"]) (object["geometry"] as BufferGeometry).computeVertexNormals();

                //Calculate OBB
                if (props.onOBBCalculated && object["geometry"]) {
                    let geometry = (object["geometry"] as BufferGeometry).toNonIndexed();
                    let position = geometry.getAttribute('position');
                    let points = new Array<YUKAVec3>();
                    for (let i = 0; i < position.count; i++) {

                        const x = position.getX(i);
                        const y = position.getY(i);
                        const z = position.getZ(i);

                        points.push(new YUKAVec3(x, y, z));
                    }
                    const yOBB = new YUKAOBB().fromPoints(points);
                    const tOBB = new OBB(
                        new Vector3(yOBB.center.x, yOBB.center.y, yOBB.center.z),
                        new Vector3(yOBB.halfSizes.x, yOBB.halfSizes.y, yOBB.halfSizes.z));
                    let rotation = [];
                    yOBB.rotation.toArray(rotation);
                    tOBB.rotation.fromArray(rotation);
                    props.onOBBCalculated(tOBB, object);
                }
            }
        });
        return clonedMesh;
    }, [gltf, props.color]);

    return (
        <primitive onClick={props.onClick} object={modelClone} scale={props.scale} position={props.position} rotation={[
            MathUtils.degToRad(props.rotation ? props.rotation[0] : 0),
            MathUtils.degToRad(props.rotation ? props.rotation[1] : 0),
            MathUtils.degToRad(props.rotation ? props.rotation[2] : 0),
            props.rotation ? props.rotation[3] : "XYZ"
        ]} />
    );
}

export default MeshLoader
