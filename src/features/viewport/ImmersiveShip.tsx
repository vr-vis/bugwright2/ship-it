import { Interactive, useController, useXR } from "@react-three/xr";
import { useEffect, useLayoutEffect, useRef, useState } from "react";
import { ColorRepresentation, DoubleSide, Vector3 } from "three";
import StreamedMesh from './Mesh';
import { useStore } from '../../common/store';
import { Text } from '@react-three/drei';

function Polygon(props: { opacity?: number, points: Vector3[], color?: ColorRepresentation, text?: string }) {
  const ref = useRef<any>();
  const [center2, setCenter] = useState(new Vector3());

  useLayoutEffect(() => {
    const center = new Vector3();
    for (const p of props.points) {
      center.add(p);
    }
    center.divideScalar(props.points.length);
    setCenter(center);

    const points: Vector3[] = [];
    for (let i = 0; i < props.points.length; ++i) {
      points.push(center);
      points.push(props.points[i]);
      points.push(props.points[(i + 1) % props.points.length]);
    }

    ref.current.geometry.setFromPoints(points);
  }, [ref, props.points])

  return (
    <group position={[0, 0, -0.2]}>
      <mesh ref={ref} >
        <bufferGeometry />
        <meshStandardMaterial color={props.color} transparent opacity={props.opacity || 1} side={DoubleSide} />
      </mesh>
      <Text color="black" position={[center2.x, center2.y, center2.z - 1]} rotation={[0, Math.PI, 0]} scale={3}>
        {props.text}
      </Text>
    </group>
  )
}

export interface ImmersiveShipProps {
  selectedRobot?: string;
  isControlling: boolean;
  finishedControlling: () => void;
}

const ImmersiveShip = (props: ImmersiveShipProps) => {
  const ship = useStore(state => state.ship);
  const [points, setPoints] = useState<Vector3[]>([]);
  const [nextP, setNextP] = useState<Vector3>();
  const [hoveringLast, setHoveringLast] = useState(false);
  const [pointsComplete, setPointsComplete] = useState(false);
  const sphereSize = 0.1;

  if (!ship || !ship.topics || !ship.topics.mesh) {
    return null;
  }
  if (!props.isControlling) {
    return (
      <StreamedMesh
        rosbridgeURI={ship.rosbridge.uri}
        topic={ship.topics?.streamed_mesh ? ship.topics.streamed_mesh : ""}
      />
    );
  }

  return (
    <>
      <Interactive
        onSelect={event => {
          if (event.intersection && !pointsComplete && !hoveringLast) {
            const p = event.intersection.point;
            setPoints(points => [...points, p]);
          }
        }}
        onMove={event => setNextP(event.intersection?.point)}
        onBlur={() => setNextP(undefined)}
      >
        <StreamedMesh
          rosbridgeURI={ship.rosbridge.uri}
          topic={ship.topics?.streamed_mesh ? ship.topics.streamed_mesh : ""}
        />
      </Interactive>
      {
        (nextP && !hoveringLast && !pointsComplete ? [...points, nextP] : points).map((p, i) =>
          !pointsComplete && points.length > 2 && i === 0 ?
            <Interactive
              onHover={() => setHoveringLast(true)}
              onBlur={() => setHoveringLast(false)}
              onSelect={() => setPointsComplete(true)}
            >
              <mesh position={p}>
                <sphereGeometry args={[sphereSize * 3]} />
                <meshBasicMaterial color={hoveringLast ? "green" : "red"} />
              </mesh>
            </Interactive> :
            <mesh position={p}>
              <sphereGeometry args={[sphereSize]} />
              <meshBasicMaterial color={pointsComplete ? "green" : "red"} />
            </mesh>
        )
      }
      {
        pointsComplete ?
          <Interactive
            onSelect={() => {
              setPoints([]);
              setNextP(undefined);
              setHoveringLast(false);
              setPointsComplete(false);
              props.finishedControlling();
            }}
          >
            <Polygon
              points={nextP && !hoveringLast && !pointsComplete ? [...points, nextP] : points}
              opacity={0.7}
              color="green"
              text="Click to start mission"
            />
          </Interactive>
          :
          points.length > 2 ?
            <Polygon
              points={nextP && !hoveringLast && !pointsComplete ? [...points, nextP] : points}
              color="blue"
              opacity={0.5}
            /> : null
      }
    </>
  );
}

export default ImmersiveShip
