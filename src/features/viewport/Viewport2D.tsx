import { Canvas } from '@react-three/fiber';
import { useEffect, useRef, useState } from 'react';
import { useStore } from '../../common/store';
import For from '../../common/For';
import Robot from './Robot';
import { OrthographicCamera } from '@react-three/drei';
import { Box } from '@mui/system';
import LoopIcon from '@mui/icons-material/Loop';
import { IconButton } from '@mui/material';
import StreamedMesh from './Mesh';
import TF2 from './TF2';
import ShipModel from '../ship/ShipModel';
import Water from './Water';
import Skybox from './SkyBox';
import { Group, Matrix4, Object3D, Vector3 } from 'three';
import { OBB } from "three-stdlib";

const lineHeight = 20; // Magic value for now to get more reasonable delta values if the delta mode is pixels.
const scrollSpeed = 1.2; // Crolling by one line increases the scale by this factor.

export type ShipSide = 'portside' | 'starboard';

const Viewport2D = () => {
  const robots = useStore(state => state.robots);
  const ship = useStore(state => state.ship);
  const [side, setSide] = useState<ShipSide>('portside');
  const [transformedOBB, setTransformedOBB] = useState<OBB | null>(null);
  const [scale, setScale] = useState(1.0);
  const [distanceToShip, setDistanceToShip] = useState(3.5);
  const cameraAlignment = useRef<Group | null>(null);
  const [rightButtonDown, setRightButtonDown] = useState(false);
  const [cameraPosition, setCameraPosition] = useState<[number, number]>([0, 0]);
  const sideFactor = side === 'portside' ? -1 : 1;
  const showOcean = useStore(state => state.appSettings.showSea);
  const oceanLevelOffest = useStore(state => state.appSettings.oceanLevelOffset);
  const canvas = useRef<HTMLCanvasElement>(null);

  // React uses passive events by default. So, we need to register the event
  // listener this way.
  useEffect(() => {
    const currentCanvas = canvas.current;
    if (currentCanvas) {
      const listener = (event: WheelEvent) => {
        let delta = 0;
        switch (event.deltaMode) {
          case WheelEvent.DOM_DELTA_PIXEL:
            delta = event.deltaY / lineHeight;
            break;

          case WheelEvent.DOM_DELTA_LINE:
          case WheelEvent.DOM_DELTA_PAGE: // WTF?
            delta = event.deltaY;
            break;
        }
        setScale(scale => scale * Math.pow(scrollSpeed, delta));
        event.stopPropagation();
        event.preventDefault();
      };
      currentCanvas.addEventListener('wheel', listener, { passive: false });

      return () => {
        currentCanvas.removeEventListener('wheel', listener);
      };
    }
  }, [canvas]);

  useEffect(() => {
    if (!cameraAlignment.current || !transformedOBB) return;

    cameraAlignment.current.position.copy(transformedOBB.center);

    let axisRight = new Vector3(1, 0, 0).applyMatrix3(transformedOBB.rotation);
    //Longest halfsize will be X axis in view
    if (transformedOBB.halfSize.z > transformedOBB.halfSize.x) {
      axisRight = new Vector3(0, 0, -1).applyMatrix3(transformedOBB.rotation);
    }

    setDistanceToShip(Math.min(transformedOBB.halfSize.x, transformedOBB.halfSize.z) * 2);

    cameraAlignment.current.setRotationFromMatrix(new Matrix4().makeBasis(axisRight, Object3D.DefaultUp, axisRight.clone().cross(Object3D.DefaultUp)));
    console.log(Math.min(transformedOBB.halfSize.x, transformedOBB.halfSize.z) * 2);
  }, [cameraAlignment, transformedOBB]);

  return (
    <Box
      component="div"
      sx={{
        position: 'relative',
        height: '100%',
      }}
    >
      <Canvas
        ref={canvas}
        tabIndex={0}
        style={{
        }}
        onMouseEnter={event => {
          if ((event.buttons & 2) === 0) {
            setRightButtonDown(false);
          }
        }}
        onMouseMove={event => {
          if (rightButtonDown) {
            setCameraPosition(position => [
              position[0] + event.movementX * scale,
              position[1] + event.movementY * scale,
            ]);
          }
        }}
        onMouseDown={event => {
          if (event.button === 2) {
            setRightButtonDown(true);
          }
        }}
        onMouseUp={event => {
          if (event.button === 2) {
            setRightButtonDown(false);
          }
        }}
        onContextMenu={event => {
          setRightButtonDown(true);
          event.stopPropagation();
          event.preventDefault();
        }}
      >
        <group ref={cameraAlignment}>
          <OrthographicCamera
            makeDefault
            scale={scale}
            position={[sideFactor * cameraPosition[0], cameraPosition[1], sideFactor * -1 * distanceToShip]}
            rotation={[0, side === 'portside' ? 0 : Math.PI, 0]}
            near={0}
            far={10000}
          />
        </group>
        <ambientLight />
        <ambientLight />
        <pointLight />
        <Water waterNormalsTexture='waternormals.jpg' size={1000} heightOffset={oceanLevelOffest} visible={showOcean} />

        <ShipModel onOBBCalculated={(obb: OBB, parent: Object3D) => {
          setTransformedOBB(obb.clone().applyMatrix4(parent.matrixWorld));
        }} />

        <For each={Object.values(robots)}>
          {
            robot =>
              <Robot
                config={robot}
                key={robot.name}
              />
          }
        </For>
        <TF2 />
      </Canvas>
      <Box
        component="div"
        sx={{
          position: 'absolute',
          top: 0,
          right: 0,
        }}
      >
        <IconButton
          onClick={() => setSide(side => side === 'starboard' ? 'portside' : 'starboard')}
        >
          <LoopIcon />
        </IconButton>
      </Box>
    </Box>
  );
}

export default Viewport2D;
