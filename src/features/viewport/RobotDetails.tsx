import { Text } from '@react-three/drei';
import { PropsWithoutRef, useEffect, useState } from 'react';
import { DoubleSide, Texture } from 'three';
import { useTopic } from '../../common/store';
import { Robot as RobotConfig } from '../../schemas/Robot.schema';

export interface RobotDetailsProps {
  config: RobotConfig;
}

export default function RobotDetails(props: PropsWithoutRef<RobotDetailsProps>) {
  const [activeImageStream, setActiveImageStream] = useState(0);
  const [image] = useState(new Image());
  const [texture] = useState(new Texture(image));

  const compressedImage = useTopic(props.config.rosbridge.uri, props.config.topics.images?.at(activeImageStream)?.topic, 'sensor_msgs/CompressedImage');

  useEffect(() => {
    image.onload = () => {
      texture.needsUpdate = true;
    }
  }, [image, texture]);

  useEffect(() => {
    if (compressedImage) {
      image.src = `data:image/${compressedImage.format};base64, ${compressedImage.data}`;
    }
  }, [compressedImage, image]);

  return (
    <group rotation={[0, Math.PI, 0]}>
      <group position={[0, 1 + 0.25, 0]}>
        <mesh>
          <planeGeometry args={[2, 0.5]} />
          <meshStandardMaterial color="black" side={DoubleSide} transparent opacity={0.7} />
        </mesh>
        <Text lineHeight={1} scale={2}>
          { props.config.name } (100%)
        </Text>
      </group>
      <mesh>
        <planeGeometry args={[2, 2]} />
        <meshBasicMaterial color="white" side={DoubleSide} map={texture} />
      </mesh>
    </group>
  );
}

