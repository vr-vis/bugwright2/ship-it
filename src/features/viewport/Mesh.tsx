import { EventHandlers } from "@react-three/fiber/dist/declarations/src/core/events";
import { useEffect, useMemo, useState } from "react";
import { BufferGeometry, DoubleSide, Float32BufferAttribute, Uint32BufferAttribute } from "three";
import { Messages } from "../../common/ROS";
import { useService } from "../../common/service";
import mesh_msgs from "../../common/ROS/mesh_msgs";
import { useTopic } from "../../common/store";


export interface StreamedMeshProps extends EventHandlers {
    rosbridgeURI: string;
    topic: string;
    onGeometryUpdate?: (geometry?: BufferGeometry) => void;
}

const StreamedMesh = (props: StreamedMeshProps) => {

    const getGeometry = useService(props.rosbridgeURI, props.topic, 'GetGeometry');
    const [mesh, setMesh] = useState<mesh_msgs['MeshGeometryStamped']>();

    useEffect(() => {
        console.log("UseEffect");
        if (mesh) return;
        getGeometry({ uuid: "" })
            .then((value: { mesh_geometry_stamped: mesh_msgs['MeshGeometryStamped'] }) => { setMesh(value.mesh_geometry_stamped); })
            .catch((reason) => { console.log("Error loading Mesh: " + reason); });
    }, [getGeometry]);

    // const mesh2 = useTopic(props.rosbridgeURI, props.topic, 'mesh_msgs/MeshGeometryStamped');
    const [geometry, setGeometry] = useState<BufferGeometry>();

    useEffect(() => {
        if (mesh) {
            const geometry = new BufferGeometry();
            if ('triangles' in mesh && 'vertices' in mesh) {
                // Hack to support shape_msgs/Mesh messages
                const meshShape = mesh as Messages['shape_msgs']['Mesh'];
                const vertices = new Float32Array(meshShape.vertices.length * 3);
                const indices = new Uint32Array(meshShape.triangles.length * 3);

                meshShape.vertices.forEach((vertex, index) => {
                    vertices[index * 3 + 0] = -vertex.x;
                    vertices[index * 3 + 2] = vertex.y;
                    vertices[index * 3 + 1] = vertex.z;
                });

                meshShape.triangles.forEach((triangle, index) => {
                    indices[index * 3 + 0] = triangle.vertex_indices[0];
                    indices[index * 3 + 1] = triangle.vertex_indices[1];
                    indices[index * 3 + 2] = triangle.vertex_indices[2];
                });
                geometry.setIndex(new Uint32BufferAttribute(indices, 1));
                geometry.setAttribute('position', new Float32BufferAttribute(vertices, 3));
            } else {
                const vertices = new Float32Array(mesh.mesh_geometry.vertices.length * 3);
                const indices = new Uint32Array(mesh.mesh_geometry.faces.length * 3);

                mesh.mesh_geometry.vertices.forEach((vertex, index) => {
                    vertices[index * 3 + 0] = -vertex.x;
                    vertices[index * 3 + 2] = vertex.y;
                    vertices[index * 3 + 1] = vertex.z;
                });

                mesh.mesh_geometry.faces.forEach((face, index) => {
                    indices[index * 3 + 0] = face.vertex_indices[0];
                    indices[index * 3 + 1] = face.vertex_indices[1];
                    indices[index * 3 + 2] = face.vertex_indices[2];
                });
                geometry.setIndex(new Uint32BufferAttribute(indices, 1));
                geometry.setAttribute('position', new Float32BufferAttribute(vertices, 3));
            }
            geometry.computeVertexNormals();
            setGeometry(geometry);
            if (props.onGeometryUpdate) {
                props.onGeometryUpdate(geometry);
            }
        } else {
            setGeometry(undefined);
            if (props.onGeometryUpdate) {
                props.onGeometryUpdate(undefined);
            }
        }
    }, [mesh]);

    return (
        <mesh
            onClick={props.onClick}
            onContextMenu={props.onContextMenu}
            onDoubleClick={props.onDoubleClick}
            onPointerUp={props.onPointerUp}
            onPointerDown={props.onPointerDown}
            onPointerOver={props.onPointerOver}
            onPointerOut={props.onPointerOut}
            onPointerEnter={props.onPointerEnter}
            onPointerLeave={props.onPointerLeave}
            onPointerMove={props.onPointerMove}
            onPointerMissed={props.onPointerMissed}
            onPointerCancel={props.onPointerCancel}
            onWheel={props.onWheel}
            geometry={geometry}
        >
            <meshPhongMaterial side={DoubleSide} />
        </mesh>
    );
}

export default StreamedMesh
