import { Text } from "@react-three/drei";
import { useFrame } from "@react-three/fiber";
import { useController, useXR } from "@react-three/xr";
import { useEffect, useState } from "react";
import { Vector3, Vector4 } from "three";

export interface FlyingControlsProps {
}

const FlyingControls = (props: FlyingControlsProps) => {
  const rightController = useController('right')
  const { player } = useXR();
  const [debugText, setDebugText] = useState("");

  useEffect(() => {
    player.position.z = 15;
    player.rotation.y = Math.PI;
  }, []);

  useFrame(frame => {
    if (rightController && rightController.inputSource.gamepad) {
      const forwardAxis = 3;
      const sidewardAxis = 2;
      // rightController.controller.
      const movementVector = new Vector4(-rightController.inputSource.gamepad.axes[sidewardAxis], 0, -rightController.inputSource.gamepad.axes[forwardAxis], 0);
      movementVector.applyMatrix4(rightController.controller.matrixWorld);
      movementVector.applyMatrix4(player.matrixWorld.clone().invert());
      player.position.add(new Vector3(movementVector.x * 0.5, -movementVector.y * 0.5, movementVector.z * 0.5));
    }
  });


  return (
    <Text color="black" position={[0, 1, 18]} rotation={[0, Math.PI, 0]}>
      { debugText }
    </Text>
  );
}

export default FlyingControls
