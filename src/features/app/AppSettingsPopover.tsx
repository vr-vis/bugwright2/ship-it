import PopoverListButton from "../../common/PopoverListButton";
import { DarkMode, Label, LightMode, Settings } from '@mui/icons-material';
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import { useStore } from "../../common/store";
import { FormControlLabel, ListItem, Slider, Switch, Typography } from "@mui/material";

export interface AppSettingsPopoverProps {
}

const AppSettingsPopover = (props: AppSettingsPopoverProps) => {
  const darkMode = useStore(state => state.appSettings.darkTheme);
  const showSea = useStore(state => state.appSettings.showSea);
  const showSkybox = useStore(state => state.appSettings.showSkybox);
  const showTFTreeVis = useStore(state => state.appSettings.showTFDebugVis);
  const showTFTreeLabels = useStore(state => state.appSettings.showTFDebugLabels);
  const oceanLevelOffset = useStore(state => state.appSettings.oceanLevelOffset);
  const updateSettings = useStore(state => state.updateAppSettings);

  return (
    <PopoverListButton title={"Settings"} icon={Settings}>
      <ListItem button={false}>
        <FormControlLabel control={
          <Switch
            checked={darkMode}
            onChange={() => updateSettings({ darkTheme: !darkMode })}
          />
        } label={darkMode ? "Dark Mode" : "Light Mode"} />
      </ListItem>

      <ListItem button={false}>
        <FormControlLabel control={
          <Switch
            checked={showSea}
            onChange={() => updateSettings({ showSea: !showSea })}
          />
        } label="Show Ocean" />
      </ListItem>

      <ListItem button={false}>
        <Slider
          value={oceanLevelOffset}
          onChange={(event, value) => updateSettings({ oceanLevelOffset: (value as number) })}
          aria-labelledby="Ocean Level Offset"
          max={5}
          min={-5}
          step={0.01}
          valueLabelDisplay="auto"
        />
        <Typography gutterBottom noWrap>
          Ocean Height Offset
        </Typography>
      </ListItem>

      <ListItem button={false}>
        <FormControlLabel control={
          <Switch
            checked={showSkybox}
            onChange={() => updateSettings({ showSkybox: !showSkybox })}
          />
        } label="Show Skybox" />
      </ListItem>

      <ListItem button={false}>
        <FormControlLabel control={
          <Switch
            checked={showTFTreeVis}
            onChange={() => updateSettings({ showTFDebugVis: !showTFTreeVis })}
          />
        } label="Show TF-Tree Visualization" />
      </ListItem>

      <ListItem button={false}>
        <FormControlLabel control={
          <Switch
            checked={showTFTreeLabels}
            onChange={() => updateSettings({ showTFDebugLabels: !showTFTreeLabels })}
          />
        } label="Show TF-Tree Labels" />
      </ListItem>
    </PopoverListButton>
  );
};

export default AppSettingsPopover;
