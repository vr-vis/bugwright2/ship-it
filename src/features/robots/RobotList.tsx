import { PropsWithoutRef, useCallback, useState } from 'react';
import { useAppSelector } from '../../app/hooks';
import { Robot as RobotConfig } from '../../schemas/Robot.schema';
import For from '../../common/For';
import { Box } from '@mui/system';
import { Button, IconButton, MobileStepper, Paper, useTheme } from '@mui/material';
import { Close as CloseIcon } from '@mui/icons-material';
import Show from '../../common/Show';
import RobotDetailsDialog from './RobotDetailsDialog';
import InfoIcon from '@mui/icons-material/Info';
import BatteryIcon from './BatteryIcon';
import { useStore, useTopic } from '../../common/store';
import CompressedROSImage from '../images/CompressedImage';
import ImageCarousel from '../images/ImageCarousel';
import RobotMissionPlanningDialog from './RobotMissionPlan';

interface RobotItemProps {
  robot: RobotConfig;
}

function RobotItem(props: PropsWithoutRef<RobotItemProps>) {
  const [showDetailView, setShowDetailView] = useState(false);
  const [showMissionPlanning, setShowMissionPlanning] = useState(false);

  const batteryState = useTopic(props.robot.rosbridge.uri, props.robot.topics.batteryState, 'sensor_msgs/BatteryState');

  let borderColor = 'transparent';

  return (
    <Box
      component="div"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        borderWidth: '1px',
        borderColor: borderColor,
        borderStyle: 'solid',
      }}
    >
      <Show when={showMissionPlanning}>
        <RobotMissionPlanningDialog
          robot={props.robot.name}
          close={() => setShowMissionPlanning(false)}
        />
      </Show>
      <Show when={showDetailView}>
        <RobotDetailsDialog
          robot={props.robot.name}
          close={() => setShowDetailView(false)}
        />
      </Show>
      <Box
        component="div"
        sx={{
          display: 'flex',
          flexDirection: 'column',
          marginLeft: '0.5em',
          marginRight: '0.5em',
        }}
      >
        <Box
          component="span"
          sx={{
            display: 'flex',
          }}
        >
          <Box
            component="span"
            sx={{
              flexGrow: 1,
            }}
          >
            <h3>{props.robot.name}</h3>
          </Box>
          <IconButton>
            <BatteryIcon percentage={batteryState?.percentage} />
          </IconButton>
          <IconButton
            onClick={() => setShowDetailView(true)}
          >
            <InfoIcon />
          </IconButton>
          <IconButton
            onClick={() => setShowMissionPlanning(true)}
          >
            <InfoIcon />
          </IconButton>
        </Box>
        <Box
          component="div"
          sx={{
            position: 'relative',
          }}
        >
        <Show when={props.robot.topics.images}>
        {
          images =>
            <ImageCarousel
              connection={props.robot.rosbridge.uri}
              images={images}
            />
        }
        </Show>
        </Box>
      </Box>
    </Box>
  );
}

interface RobotDetailViewProps {
  name: string;
  config: RobotConfig;
  onClose: () => void;
}

function RobotDetailView(props: PropsWithoutRef<RobotDetailViewProps>) {
  return (
    <Paper
      sx={{
        flexGrow: 1,
        overflow: 'auto'
      }}
    >
      <Box
        component="span"
        sx={{
          display: "flex",
        }}
      >
        <Box
          component="big"
          sx={{
            flexGrow: 1,
            margin: 2,
          }}
        >
          {props.name}
        </Box>
        <Button
          sx={{ mr: 2 }}
          title="Close"
          onClick={props.onClose}
        >
          <CloseIcon />
        </Button>
      </Box>
    </Paper>
  );
}

export interface RobotListProps {
}

export default function RobotList(props: PropsWithoutRef<RobotListProps>) {
  const robots = useStore(useCallback(state => state.robots, []));

  return (
    <Paper
      sx={{
        width: '100%',
        height: '100%',
        display: "flex",
        flexDirection: "column",
      }}
    >
      <h2>Robot Fleet</h2>
      <Box
        component="div"
        sx={{
          flexGrow: 1,
          minHeight: 0,
          overflowY: 'auto',
        }}
      >
        <Box 
          component="div"
          sx={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
        {
          Object.keys(robots).map(name => robots[name]).map(robot =>
            <RobotItem
              key={robot.name}
              robot={robot}
            />
          )
        }
        </Box>
      </Box>
    </Paper>
  );
}
