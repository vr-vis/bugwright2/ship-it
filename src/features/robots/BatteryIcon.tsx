import BatteryUnknownIcon from '@mui/icons-material/BatteryUnknown';
import Battery20Icon from '@mui/icons-material/Battery20';
import Battery30Icon from '@mui/icons-material/Battery30';
import Battery50Icon from '@mui/icons-material/Battery50';
import Battery60Icon from '@mui/icons-material/Battery60';
import Battery80Icon from '@mui/icons-material/Battery80';
import Battery90Icon from '@mui/icons-material/Battery90';
import BatteryFullIcon from '@mui/icons-material/BatteryFull';
import BatteryCharging20Icon from '@mui/icons-material/BatteryCharging20';
import BatteryCharging30Icon from '@mui/icons-material/BatteryCharging30';
import BatteryCharging50Icon from '@mui/icons-material/BatteryCharging50';
import BatteryCharging60Icon from '@mui/icons-material/BatteryCharging60';
import BatteryCharging80Icon from '@mui/icons-material/BatteryCharging80';
import BatteryCharging90Icon from '@mui/icons-material/BatteryCharging90';
import BatteryChargingFullIcon from '@mui/icons-material/BatteryChargingFull';
import BatteryAlertIcon from '@mui/icons-material/BatteryAlert';

interface BatteryIconProps {
  percentage?: number;
  isCharging?: boolean;
}

export default function BatteryIcon(props: BatteryIconProps) {
  if (typeof props.percentage === 'undefined') {
    return <BatteryUnknownIcon color="error" />;
  }

  if (props.percentage < 0) {
    return  <BatteryAlertIcon color="error" />;
  } else if (props.percentage < 0.1) {
    return props.isCharging ?
      <BatteryCharging20Icon color="warning" /> :
      <BatteryAlertIcon color="warning" />;
  } else if (props.percentage < 0.25) {
    return props.isCharging ?
      <BatteryCharging20Icon color="success" /> :
      <Battery20Icon  />;
  } else if (props.percentage < 0.4) {
    return props.isCharging ?
      <BatteryCharging30Icon color="success" /> :
      <Battery30Icon  />;
  } else if (props.percentage < 0.55) {
    return props.isCharging ?
      <BatteryCharging50Icon color="success" /> :
      <Battery50Icon  />;
  } else if (props.percentage < 0.7) {
    return props.isCharging ?
      <BatteryCharging60Icon color="success" /> :
      <Battery60Icon  />;
  } else if (props.percentage < 0.85) {
    return props.isCharging ?
      <BatteryCharging80Icon color="success" /> :
      <Battery80Icon  />;
  } else if (props.percentage < 0.95) {
    return props.isCharging ?
      <BatteryCharging90Icon color="success" /> :
      <Battery90Icon  />;
  } else if (props.percentage <= 1.0) {
    return props.isCharging ?
      <BatteryChargingFullIcon color="success" /> :
      <BatteryFullIcon  />;
  } else { //props.percentage > 1
    return  <BatteryAlertIcon color="error" />;
  }
}
