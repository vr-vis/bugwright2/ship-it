import Dialog from '../../common/Dialog';
import { Split } from '@geoffcox/react-splitter';
import { Box, Button, useTheme } from '@mui/material';
import Viewport from '../viewport/Viewport';
import { useState } from 'react';
import { useRobot } from '../config/config-hooks';
import { useTopic } from '../../common/store';

export interface RobotDetailsDialogProps {
  robot: string;
  close: () => void;
}

const RobotDetailsDialog = (props: RobotDetailsDialogProps) => {
  const robot = useRobot(props.robot);
  const poseStamped = useTopic(robot?.rosbridge.uri, robot?.topics.pose, 'geometry_msgs/PoseStamped');
  const [showEditDialog, setShowEditDialog] = useState(false);

  return (
    <Dialog
      open={!!robot}
      title={props.robot}
      close={props.close}
      width="90%"
      height="85%"
    >
      <Split
        initialPrimarySize='20%'
      >
        <Box
          component="div"
          sx={{
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <Box 
            component="div"
            sx={{
              whiteSpace: 'nowrap',
            }}
          >
            Position:
            &emsp;
            { Math.round((poseStamped?.pose?.position.x || 0) * 100) / 100 } 
            &nbsp;
            { Math.round((poseStamped?.pose?.position.y || 0) * 100) / 100 } 
            &nbsp;
            { Math.round((poseStamped?.pose?.position.z || 0) * 100) / 100 } 
            <br />
            Orientation:
            &emsp;
            { Math.round((poseStamped?.pose?.orientation.w || 0) * 100) / 100 } 
            &nbsp;
            { Math.round((poseStamped?.pose?.orientation.x || 0) * 100) / 100 } 
            &nbsp;
            { Math.round((poseStamped?.pose?.orientation.y || 0) * 100) / 100 } 
            &nbsp;
            { Math.round((poseStamped?.pose?.orientation.z || 0) * 100) / 100 } 
          </Box>
          <Box
            component="div"
            sx={{
              flexGrow: 1,
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <h2>Event Log</h2>
            <Box
              component="div"
              sx={{
                flexGrow: 1,
              }}
            >
              Go to position (0, 100, 12) <br/>
              Inspection Started <br/>
              Inspection paused <br/>
              Inspection resumed
            </Box>
          </Box>
          <Box
            component="div"
            sx={{
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <Button
              sx={{
                flexGrow: 1,
              }}
            >
              Control Robot
            </Button>
            <Button
              sx={{
                flexGrow: 1,
              }}
            >
              Stop
            </Button>
          </Box>
          <Button
            onClick={() => setShowEditDialog(true)}
          >
            Edit
          </Button>
        </Box>

        <Split
          initialPrimarySize='50%'
          horizontal
        >
          <Viewport />
          {/*<ImageCarousel
            connectionName={robot.connection}
            prefix={robot.prefix}
            sx={{
              height: "100%",
            }}
          />*/}
        </Split>
      </Split>
    </Dialog>
  );
};

export default RobotDetailsDialog;
