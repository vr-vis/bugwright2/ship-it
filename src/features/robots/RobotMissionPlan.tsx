import Dialog from '../../common/Dialog';
import { Split } from '@geoffcox/react-splitter';
import { Box, Button, FormControl, IconButton, InputAdornment, InputLabel, MenuItem, Paper, Select, TextField, useTheme } from '@mui/material';
import Viewport from '../viewport/Viewport';
import { useEffect, useId, useRef, useState } from 'react';
import { useStore, useTopic } from '../../common/store';
import { useService } from '../../common/service';
import { Mission, MissionType } from '../../common/BugWright2';
import For from '../../common/For';
import { Add, Delete } from '@mui/icons-material';
import { BufferAttribute, BufferGeometry, DoubleSide, Matrix3, Plane, Vector3 } from 'three';
import { ThreeEvent } from '@react-three/fiber';
import convexHull from 'monotone-convex-hull-2d';

const colors = ["DarkBlue", "DarkCyan", "DarkGoldenRod", "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", "DarkOrange", "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", "DarkSlateGray", "DarkTurquoise", "DarkViolet", "DeepPink"];

export interface RobotMissionProps {
  mission: Mission;
  onChange: (mission: Mission | null) => void;
  index: number;
  onHoverStart: () => void;
  onHoverEnd: () => void;
  onSelect: () => void;
  selected: boolean;
  hovered: boolean;
}

const RobotMission = (props: RobotMissionProps) => {
  const id = useId();

  return (
    <Paper
      sx={{
        backgroundColor: colors[props.index % colors.length],
        opacity: props.hovered || props.selected ? 1.0 : 0.5,
      }}
      onMouseEnter={props.onHoverStart}
      onMouseLeave={props.onHoverEnd}
    >
      <h4
        style={{
          margin: "0.2em 0",
        }}
      >
        Mission {props.index}
        <IconButton onClick={() => props.onChange(null)}>
          <Delete />
        </IconButton>
      </h4>
      <FormControl fullWidth>
        <InputLabel id={`${id}-type-label`}>Type</InputLabel>
        <Select
          label="Type"
          labelId={`${id}-type-label`}
          value={MissionType[props.mission.mission_type]}
          onChange={event => props.onChange({ ...props.mission, mission_type: MissionType[event.target.value as keyof typeof MissionType] })}
        >
          <MenuItem value={MissionType[MissionType.INSPECT_POINT]}>Inspect Point</MenuItem>
          <MenuItem value={MissionType[MissionType.INSPECT_PATH]}>Inspect Path</MenuItem>
          <MenuItem value={MissionType[MissionType.INSPECT_AREA]}>Inspect Area</MenuItem>
        </Select>
        <h5
          style={{
            margin: "0.1em 0",
          }}
        >
          Points
          <IconButton>
            <Add
              onClick={() => props.onChange({ mission_type: props.mission.mission_type, points: [...props.mission.points, { x: 0, y: 0, z: 0 }] })}
            />
          </IconButton>
        </h5>
        <ol>
          <For each={props.mission.points}>
            {
              (p, index) =>
                <li key={`${index}`}>
                  <Box
                    component="div"
                    sx={{
                      display: 'flex',
                      flexDirection: 'row',
                    }}
                  >
                    <TextField
                      size="small"
                      type="number"
                      InputProps={{
                        startAdornment: <InputAdornment position="start">x</InputAdornment>,
                      }}
                      value={p.x.toString()}
                      onChange={event => props.onChange({ ...props.mission, points: props.mission.points.map((p, i) => i === index ? { ...p, x: parseFloat(event.target.value) } : p) })}
                    />
                    <TextField
                      size="small"
                      type="number"
                      InputProps={{
                        startAdornment: <InputAdornment position="start">y</InputAdornment>,
                      }}
                      value={p.y.toString()}
                      onChange={event => props.onChange({ ...props.mission, points: props.mission.points.map((p, i) => i === index ? { ...p, y: parseFloat(event.target.value) } : p) })}
                    />
                    <TextField
                      size="small"
                      type="number"
                      InputProps={{
                        startAdornment: <InputAdornment position="start">z</InputAdornment>,
                      }}
                      value={p.z.toString()}
                      onChange={event => props.onChange({ ...props.mission, points: props.mission.points.map((p, i) => i === index ? { ...p, z: parseFloat(event.target.value) } : p) })}
                    />

                    <IconButton
                      onClick={() => props.onChange({ ...props.mission, points: props.mission.points.filter((_, i) => i !== index) })}
                    >
                      <Delete />
                    </IconButton>
                  </Box>
                </li>
            }
          </For>
        </ol>
      </FormControl>
    </Paper>
  );
}

export interface RobotMissionVisualizerProps {
  mission: Mission;
  color: string;
  hovered: boolean;
  selected: boolean;
  onHoverStart: () => void;
  onHoverEnd: () => void;
  onSelect: () => void;
}

const RobotMissionVisualizer = (props: RobotMissionVisualizerProps) => {
  const [geometry, setGeometry] = useState<BufferGeometry | undefined>();

  useEffect(() => {
    const geometry = new BufferGeometry();

    switch (props.mission.mission_type) {
      case MissionType.INSPECT_POINT: {
        const positions = new Float32Array(props.mission.points.length * 3);
        for (let i = 0; i < props.mission.points.length; i++) {
          positions[i * 3 + 0] = props.mission.points[i].x;
          positions[i * 3 + 1] = props.mission.points[i].y;
          positions[i * 3 + 2] = props.mission.points[i].z;
        }
        geometry.setAttribute('position', new BufferAttribute(positions, 3));
        break;
      }

      case MissionType.INSPECT_PATH: {
        const positions = new Float32Array(2 * (props.mission.points.length - 1) * 3);
        for (let i = 0; i < props.mission.points.length - 1; i++) {
          positions[i * 6 + 0] = props.mission.points[i].x;
          positions[i * 6 + 1] = props.mission.points[i].y;
          positions[i * 6 + 2] = props.mission.points[i].z;

          positions[i * 6 + 3] = props.mission.points[i + 1].x;
          positions[i * 6 + 4] = props.mission.points[i + 1].y;
          positions[i * 6 + 5] = props.mission.points[i + 1].z;
        }
        geometry.setAttribute('position', new BufferAttribute(positions, 3));
        break;
      }

      case MissionType.INSPECT_AREA: {
        const center = new Vector3();
        for (const p of props.mission.points) {
          center.add(new Vector3(p.x, p.y, p.z));
        }
        center.divideScalar(props.mission.points.length);

        const positions = new Float32Array(props.mission.points.length * 3 * 3);
        for (let i = 0; i < props.mission.points.length; ++i) {
          positions[i * 9 + 0] = center.x;
          positions[i * 9 + 1] = center.y;
          positions[i * 9 + 2] = center.z;

          positions[i * 9 + 3] = props.mission.points[i].x;
          positions[i * 9 + 4] = props.mission.points[i].y;
          positions[i * 9 + 5] = props.mission.points[i].z;

          positions[i * 9 + 6] = props.mission.points[(i + 1) % props.mission.points.length].x;
          positions[i * 9 + 7] = props.mission.points[(i + 1) % props.mission.points.length].y;
          positions[i * 9 + 8] = props.mission.points[(i + 1) % props.mission.points.length].z;
        }
        geometry.setAttribute('position', new BufferAttribute(positions, 3));
        break;
      }
    }

    setGeometry(geometry);
  }, [props]);

  switch (props.mission.mission_type) {
    case MissionType.INSPECT_POINT:
      return (
        <points geometry={geometry}>
          <pointsMaterial size={0.1} color={props.color} />
        </points>
      );

    case MissionType.INSPECT_PATH:
      return (
        <lineSegments geometry={geometry}>
          <lineBasicMaterial color={props.color} linewidth={5} depthTest={false} />
        </lineSegments>
      );

    case MissionType.INSPECT_AREA:
      return (
        <mesh geometry={geometry} onClick={() => props.onSelect()} onPointerOver={() => props.onHoverStart()} onPointerOut={() => props.onHoverEnd()}>
          <meshBasicMaterial transparent color={props.color} side={DoubleSide} polygonOffset polygonOffsetFactor={0} polygonOffsetUnits={-1000} opacity={props.hovered || props.selected ? 1.0 : 0.5} />
        </mesh>
      );
  }
}

export interface RobotMissionPlanningDialogProps {
  robot: string;
  close: () => void;
}

const RobotMissionPlanningDialog = (props: RobotMissionPlanningDialogProps) => {
  const robot = useStore(state => state.robots[props.robot]);

  const prepareMission = useService(robot.rosbridge.uri, robot.services?.prepareMission, 'PrepareMission');
  const executeMission = useService(robot.rosbridge.uri, robot.services?.executeMission, 'ExecuteMission');
  const [missions, setMissions] = useState<Mission[]>([]);
  const [hoveredId, setHoveredId] = useState<number>();
  const [selectedId, setSelectedId] = useState<number>();
  const [preparedId, setPreparedId] = useState<number>();

  const onClick = (event: ThreeEvent<MouseEvent>) => {
    if (event.nativeEvent.altKey) {
      if (missions.length === 0 || event.nativeEvent.shiftKey) {
        setSelectedId(missions.length);
        setMissions([
          ...missions,
          {
            mission_type: MissionType.INSPECT_POINT,
            points: [{ x: event.point.x, y: event.point.y, z: event.point.z }],
          }
        ]);
      } else {
        if (typeof selectedId !== 'number') {
          setSelectedId(missions.length - 1);
        }
        const selectedMission = missions[typeof selectedId === 'number' ? selectedId : missions.length - 1];
        // const lastMission = missions[missions.length - 1];
        selectedMission.points.push({ x: event.point.x, y: event.point.y, z: event.point.z });
        if (selectedMission.points.length === 3) {
          selectedMission.mission_type = MissionType.INSPECT_AREA;
        } else if (selectedMission.points.length > 1 && selectedMission.mission_type === MissionType.INSPECT_POINT) {
          selectedMission.mission_type = MissionType.INSPECT_PATH;
        }

        if (selectedMission.mission_type === MissionType.INSPECT_AREA && selectedMission.points.length > 3) {
          const points = selectedMission.points.map(p => new Vector3(p.x, p.y, p.z));
          const plane = new Plane();

          plane.setFromCoplanarPoints(points[0], points[1], points[2]);
          const x = new Vector3().subVectors(points[1], points[0]).normalize();
          const y = new Vector3().crossVectors(plane.normal, x).normalize();
          const z = plane.normal.clone();

          const rotationMatrix = new Matrix3();
          rotationMatrix.set(
            x.x, x.y, x.z,
            y.x, y.y, y.z,
            z.x, z.y, z.z,
          );

          const projectedPoints = points.map(p => p.projectOnPlane(plane.normal)).map(p => [p.x, p.y]);
          const hull = convexHull(projectedPoints) as number[];
          selectedMission.points = hull.map(i => selectedMission.points[i]);
        }

        setMissions([...missions]);
      }
    }
  };

  return (
    <Dialog
      open={!!robot}
      title={`${props.robot} Mission Planning`}
      close={props.close}
      width="90%"
      height="85%"
    >
      <Split>
        <Viewport onMeshClick={onClick}>
          <For each={missions}>
            {(mission, index) => <RobotMissionVisualizer
              mission={mission}
              color={colors[index % colors.length]}
              onHoverStart={() => setHoveredId(index)}
              onHoverEnd={() => hoveredId === index ? setHoveredId(undefined) : undefined}
              onSelect={() => setSelectedId(index)}
              selected={selectedId === index}
              hovered={hoveredId === index}
            />}
          </For>
        </Viewport>
        <Box
          component="div"
          sx={{
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 1,
            padding: '1.5em',
            rowGap: '1em',
            overflow: 'auto',
            height: '100%',
          }}
        >
          <h3
            style={{
              margin: 0,
            }}
          >
            Missions
            <IconButton
              onClick={() => setMissions([...missions, { mission_type: MissionType.INSPECT_POINT, points: [] }])}
            >
              <Add />
            </IconButton>
          </h3>
          {
            missions.map((mission, index) =>
              <RobotMission
                key={index}
                index={index}
                mission={mission}
                onChange={mission => mission ? setMissions(missions.map((m, i) => i === index ? mission : m)) : setMissions(missions.filter((_, i) => i !== index))}
                onHoverStart={() => setHoveredId(index)}
                onHoverEnd={() => hoveredId === index ? setHoveredId(undefined) : undefined}
                onSelect={() => setSelectedId(index)}
                selected={selectedId === index}
                hovered={hoveredId === index}
              />
            )
          }
          <Button
            onClick={async () => {
              const convertedMissions = missions.map(m => ({
                mission_type: m.mission_type,
                points: m.points.map(p => ({ x: -p.x, y: p.z, z: p.y })),
              }));

              const response = await prepareMission({ missions: convertedMissions });
              if (response.id <= 0) {
                console.error("Failed to prepare mission");
                setPreparedId(undefined);
              } else {
                setPreparedId(response.id);
              }
            }}
          >
            Prepare
          </Button>
          <Button
            disabled={!preparedId}
            onClick={async () => {
              if (!!preparedId) {
                const response = await executeMission({ id: preparedId });
                if (!response) {
                  console.error("Failed to execute mission");
                }
              }
            }}
          >
            Execute
          </Button>

        </Box>
      </Split>
    </Dialog>
  );
};

export default RobotMissionPlanningDialog;
