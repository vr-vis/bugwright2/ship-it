import PopoverListButton from "../../common/PopoverListButton";
import { DirectionsBoat as ShipIcon } from '@mui/icons-material';

const RobotsMenu = () => {
  // const shipMesh = useAppSelector(state => state.config.shipMesh);
  // const connections = 
  // const meshTopics = useTopicsOfType(

  return (
    <PopoverListButton
      icon={ShipIcon}
      title="Ship Mesh"
    >
    </PopoverListButton>
  );
};

export default RobotsMenu;
