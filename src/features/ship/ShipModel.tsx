import { useStore } from "../../common/store";
import StreamedMesh from "../viewport/Mesh";
import MeshLoader from "../viewport/MeshLoader";
import { Frame } from "../viewport/TF2";
import { Object3D } from 'three';
import { OBB } from "three-stdlib";
import { ThreeEvent } from "@react-three/fiber";

interface ShipModelProps {
    onOBBCalculated?: (obb: OBB, parent: Object3D) => void;
    onClick?: (event: ThreeEvent<MouseEvent>) => void;
}

export default function ShipModel(props: ShipModelProps) {
    const ship = useStore(state => state.ship);

    return (
        <>
            {
                /* Static Ship */
                ship && ship.static_mesh ?
                    <Frame frameId={ship.static_mesh.tf_frame}>
                        <MeshLoader
                            MeshURI={ship.static_mesh.uri}
                            rotation={ship.static_mesh.transform?.rotation}
                            position={ship.static_mesh.transform?.translation}
                            scale={ship.static_mesh.transform?.scale}
                            onOBBCalculated={props.onOBBCalculated}
                            onClick={props.onClick}
                        />
                    </Frame>
                    : null
            }
            {
                /* Streamed Ship */
                ship && ship.topics && ship.topics.streamed_mesh ?
                    <StreamedMesh
                        rosbridgeURI={ship.rosbridge.uri}
                        topic={ship.topics.streamed_mesh}
                        onClick={props.onClick}
                    />
                    : null
            }
        </>
    );
}
