import { KeyboardArrowLeft, KeyboardArrowRight } from '@mui/icons-material';
import { Box, Button, MobileStepper, Paper, SxProps, Theme } from '@mui/material';
import { CSSProperties, PropsWithoutRef, useEffect, useState } from 'react';
import Show from '../../common/Show';
import CompressedROSImage from './CompressedImage';

export interface ImageCarouselImage {
  topic: string;
  name: string;
}

export interface ImageCarouselProps {
  connection: string;
  images: ImageCarouselImage[];
  sx?: SxProps<Theme>;
}

export default function ImageCarousel(props: PropsWithoutRef<ImageCarouselProps>) {
  const [activeStep, setActiveStep] = useState(0);

  if (props.images.length === 0) {
    return null;
  }
  const image = props.images[activeStep];

  return (
    <Box
      component="div"
      sx={{
        ...props.sx,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        component="div"
        style={{
          flexGrow: 1,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <CompressedROSImage
          key={image.topic}
          connection={props.connection}
          topic={image.topic}
          style={{
            maxWidth: "100%",
          }}
        />
        <Box
          component="span"
          style={{
          }}
        >
          {image.name}
        </Box>
      </Box>
      <MobileStepper
        variant="dots"
        steps={props.images.length}
        position="static"
        activeStep={activeStep}
        sx={{
          background: 'transparent',
        }}
        nextButton={
          <Button
            size="small"
            onClick={event => {
              setActiveStep(step => step + 1);
              event.stopPropagation();
            }}
            disabled={activeStep === props.images.length - 1}
          >
            <KeyboardArrowRight />
          </Button>
        }
        backButton={
          <Button
            size="small"
            onClick={event => {
              setActiveStep(step => step - 1);
              event.stopPropagation();
            }}
            disabled={activeStep === 0}
          >
            <KeyboardArrowLeft />
          </Button>
        }
      />
    </Box>
  );
}

