import { CSSProperties } from "react";
import Show from "../../common/Show";
import { useTopic } from "../../common/store";

export interface CompressedROSImageProps {
  connection: string;
  topic: string;
  style?: CSSProperties;
}

function CompressedROSImage(props: CompressedROSImageProps) {
  const compressedImage = useTopic(props.connection, props.topic, 'sensor_msgs/CompressedImage');
  // Todo: use direct dom manipulation here!

  return (
    <Show when={compressedImage} fallback={<>Loading {props.topic}...</>}>
    {
    compressedImage =>
      <img
        style={props.style}
        src={`data:image/${compressedImage.format};base64, ${compressedImage.data}`}
        title={props.topic}
        alt={props.topic}
      />
    }
    </Show>
  );
};

export default CompressedROSImage;
