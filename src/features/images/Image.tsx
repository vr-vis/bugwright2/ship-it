import { CSSProperties } from "react";

interface RawROSImageProps {
  connectionName: string;
  topic: string;
  style?: CSSProperties;
}

// function RawROSImage(props: RawROSImageProps) {
//   const ros = useConnection(props.connectionName);
//   const image = useTopic<Image>(ros, props.topic, 'sensor_msgs/Image');
//   const [context, setContext] = useState<CanvasRenderingContext2D|null>(null);

//   let imageData: ImageData;

//   useEffect(() => {
//     if (image && context) {
//       if (!imageData || (imageData.width !== image.width || imageData.height !== image.height)) {
//         imageData = context.createImageData(image.width, image.height);
//       }
//       const data = atob(image.data);
//       switch (image.encoding) {
//       case "mono8":
//         for (let y = 0; y < image.height; ++y) {
//           for (let x = 0; x < image.width; ++x) {
//             const destinationBaseIndex = (y * image.width + x) * 4;
//             const sourceBaseIndex = y * image.step + x;

//             for (let i = 0; i < 3; ++i) {
//               imageData.data[destinationBaseIndex + i] = data.charCodeAt(sourceBaseIndex);
//             }
//             imageData.data[destinationBaseIndex + 3] = 255;
//           }
//         }
//         break;

//       case "rgb8":
//         for (let y = 0; y < image.height; ++y) {
//           for (let x = 0; x < image.width; ++x) {
//             const destinationBaseIndex = (y * image.width + x) * 4;
//             const sourceBaseIndex = y * image.step + x * 3;

//             for (let i = 0; i < 3; ++i) {
//               imageData.data[destinationBaseIndex + i] = data.charCodeAt(sourceBaseIndex + i);
//             }
//             imageData.data[destinationBaseIndex + 3] = 255;
//           }
//         }
//         break;

//       case "bgr8":
//         for (let y = 0; y < image.height; ++y) {
//           for (let x = 0; x < image.width; ++x) {
//             const destinationBaseIndex = (y * image.width + x) * 4;
//             const sourceBaseIndex = y * image.step + x * 3;

//             for (let i = 0; i < 3; ++i) {
//               imageData.data[destinationBaseIndex + i] = data.charCodeAt(sourceBaseIndex + 2 - i);
//             }
//             imageData.data[destinationBaseIndex + 3] = 255;
//           }
//         }
//         break;

//       case "16UC1":
//         for (let y = 0; y < image.height; ++y) {
//           for (let x = 0; x < image.width; ++x) {
//             const destinationBaseIndex = (y * image.width + x) * 4;
//             const sourceBaseIndex = y * image.step + x * 2;
//             const p0 = data.charCodeAt(sourceBaseIndex + 0);
//             const p1 = data.charCodeAt(sourceBaseIndex + 1);
//             const value = (p0 << 0 | p1 << 8) / Math.pow(2, 16) * 256;

//             for (let i = 0; i < 3; ++i) {
//               imageData.data[destinationBaseIndex + i] = value;
//             }
//             imageData.data[destinationBaseIndex + 3] = 255;
//           }
//         }
//         break;

//       default:
//         console.error(`Unsupported encoding: ${image.encoding}`);
//       }
//       context.putImageData(imageData, 0, 0);
//     }
//   }, [image, context]);

//   return (
//     <canvas
//       ref={canvas => setContext(canvas?.getContext("2d") || null)}
//       width={image?.width}
//       height={image?.height}
//       title={props.topic}
//       style={props.style}
//     />
//   );
// };
