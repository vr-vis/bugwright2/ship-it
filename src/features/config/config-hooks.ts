import { useAppSelector } from "../../app/hooks";

export function useRobot(name: string) {
  return useAppSelector(state => state.config.robots.find(robot => robot.name === name));
}
