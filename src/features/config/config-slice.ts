import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Robot } from '../../schemas/Robot.schema';

interface ConfigState {
  robots: Robot[];
}

const initialState: ConfigState = {
  robots: [],
}

const configSlice = createSlice({
  name: 'config',
  initialState,
  reducers: {
    updateConfig(_state, action: PayloadAction<ConfigState>) {
      return action.payload
    },
    addRobot(state, action: PayloadAction<Robot>) {
      const robotToAdd = action.payload;
      if (!state.robots.find(robot => robot.name === robotToAdd.name)) {
        state.robots.push(robotToAdd);
      }
    },
    removeRobot(state, action: PayloadAction<string>) {
      const robotIndex = state.robots.findIndex(robot => robot.name === action.payload);
      if (robotIndex !== -1) {
        state.robots.splice(robotIndex, 1);
      }
    }
  }
});

export const { addRobot, removeRobot, updateConfig } = configSlice.actions;
export default configSlice.reducer;
