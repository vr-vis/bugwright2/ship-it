import PopoverListButton from "../../common/PopoverListButton";
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { RadioButtonChecked, RadioButtonUnchecked, Videocam } from '@mui/icons-material';
import { List, ListItem, ListItemButton, ListItemText } from "@mui/material";
import { controlsChanged } from "./camera-slice";
import Show from "../../common/Show";


const CameraMenu = () => {
  const cameraControls = useAppSelector(state => state.camera.controls);
  const dispatch = useAppDispatch();

  return (
    <>
      <PopoverListButton
        icon={ Videocam }
        title="Connections"
      >
        <List>
          <ListItem>
            <ListItemText>
              Controls
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemButton
              onClick={() => dispatch(controlsChanged('orbit'))}
            >
              <Show when={cameraControls === 'orbit'} fallback={<RadioButtonUnchecked />}>
                <RadioButtonChecked />
              </Show>
              <ListItemText sx={{ marginLeft: '1em' }}>
                Orbit
              </ListItemText>
            </ListItemButton>
          </ListItem>
        </List>
      </PopoverListButton>
    </>
  );
};

export default CameraMenu;
