import { ExpandMore } from '@mui/icons-material';
import { Accordion, AccordionDetails, AccordionSummary, CircularProgress, Link, TextField } from '@mui/material';
import { useEffect, useState } from 'react';
import { Param, Ros } from 'roslib';
import For from '../../common/For';
import Show from '../../common/Show';
import Dialog from '../../common/Dialog';
import { ROSTopicState, useConnection, useTopic } from '../../common/store';

interface TopicMessageProps {
  uri: string;
  topic: ROSTopicState;
}

const TopicMessage = (props: TopicMessageProps) => {
  const message = useTopic(props.uri, props.topic.name);

  return (
    <pre
      style={{ margin: 0 }}
    >
    { JSON.stringify(message, undefined, 4) }
    </pre>
  );
};

interface TopicInfoProps {
  uri: string;
  topic: ROSTopicState;
  rosDistro?: string|null;
  searchQuery?: string,
}

const TopicInfo = (props: TopicInfoProps) => {
  const getMessageTypeURL = () => {
    const [pkg, message] = props.topic.type.split("/");
    return `http://docs.ros.org/en/${props.rosDistro || "melodic"}/api/${pkg}/html/msg/${message}.html`;
  };

  const [showMessage, setShowMessage] = useState(false);

  return (
    <Show
      when={
        !props.searchQuery ||
        props.topic.name.indexOf(props.searchQuery) !== -1 ||
        props.topic.type.indexOf(props.searchQuery) !== -1
      }
    >
      <Accordion
        onChange={(_, expanded) => setShowMessage(expanded)}
      >
        <AccordionSummary
          expandIcon={<ExpandMore />}
        >
          {props.topic.name}
          &emsp;
          <Link
            href={getMessageTypeURL()}
            target="_blank"
            sx={{ whiteSpace: "nowrap" }}
          >
            { props.topic.type }
          </Link>
          &emsp;
          ({props.topic.messageRate}Hz, {props.topic.subscriberCount} Subscriber)
        </AccordionSummary>
        <AccordionDetails>
          <Show when={showMessage}>
            <TopicMessage uri={props.uri} topic={props.topic} />
          </Show>
        </AccordionDetails>
      </Accordion>
    </Show>
  );
}

export interface ConnectionInfoDialogProps {
  connection?: string;
  onClose: () => void;
}

const ConnectionInfoDialog = (props: ConnectionInfoDialogProps) => {
  const connection = useConnection(props.connection);
  // const [rosdistro, setRosdistro] = useState<string|null>(null);
  // const [topics, setTopics] = useState<undefined|null|Topic[]>(undefined);
  // const [topicSearchQuery, setTopicSearchQuery] = useState<string|undefined>();

  // useEffect(() => {
  //   if (props.open && connection) {
  //     const rosdistro = new Param({ ros: connection, name: '/rosdistro' });
  //     rosdistro.get(rosdistro => setRosdistro(rosdistro));

  //     connection.getTopics(
  //       topics => {
  //         const topicsArray: Topic[] = [];
  //         for (let i = 0; i < topics.topics.length; ++i) {
  //           topicsArray.push({
  //             name: topics.topics[i],
  //             type: topics.types[i],
  //           });
  //         }
  //         setTopics(topicsArray);
  //       },
  //       () => {
  //         setTopics(null);
  //       }
  //     );
  //  }
  // }, [props.open, connection]);

  return (
    <Dialog
      open={!!props.connection}
      title={props.connection}
      close={props.onClose}
      width="80%"
      height="80%"
    >
    {/*
      <h2>ROS distribution</h2>
      <Show
        when={rosdistro}
        fallback={<CircularProgress />}
      >
        <p>{rosdistro}</p>
      </Show>
      <h2 style={{ display: 'flex' }}>
        <span style={{ flexGrow: 1 }}>Topics</span>
        <TextField
          variant="outlined"
          value={topicSearchQuery}
          onChange={x => setTopicSearchQuery(x.currentTarget.value)}
          margin="dense"
          placeholder="Search topics"
          sx={{
            margin: 0,
          }}
          inputProps={{
            style: {
              padding: '0.2em',
            }
          }}
        />
      </h2>
    */}
      {
        connection ?
          Object
            .keys(connection.topics)
            .map(name => ({name, topic: connection.topics[name]}))
            .map(({name, topic}) =>
            typeof topic === 'number' ?
              <p
                key={`${props.connection}${name}`}
              >
                {`${name}: ${topic}`}
              </p>
            :
              <TopicInfo
                key={`${props.connection}${name}`}
                uri={props.connection || ""}
                topic={topic}
              />
          )
        :
          null
      }
    </Dialog>
  );
};

export default ConnectionInfoDialog;
