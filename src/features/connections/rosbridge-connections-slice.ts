import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface ROSTopicState {
  name: string;
  type: string;
  value: any;
  subscriberCount: number;
}

interface RosbridgeConnectionState {
  uri: string;
  bsonMode: boolean;
  status: 'establishing connection' | 'connected' | 'disconnected';
  topics: ROSTopicState[];
}

export type RosbridgeConnectionsState = RosbridgeConnectionState[];

const initialState: RosbridgeConnectionsState = [];

const rosbridgeConnectionsSlice = createSlice({
  name: 'rosbridgeConnection',
  initialState,
  reducers: {
    connect(state, action: PayloadAction<{ uri: string, bsonMode: boolean}>) {
      const { uri, bsonMode } = action.payload;
      if (!state.find(connection => connection.uri === uri)) {
        state.push({
          uri,
          bsonMode,
          status: 'establishing connection',
          topics: [],
        });
      }
    },
    disconnect(state, action: PayloadAction<string>) {
      const uri = action.payload;
      const index = state.findIndex(connection => connection.uri === uri)
      if (index !== -1) {
        state.splice(index, 1);
      }
    },
    subscribe(state, action: PayloadAction<{ uri: string, topic: string }>) {
    },
    unsubscribe(state, action: PayloadAction<{ uri: string, topic: string }>) {
    },
  }
});

export const { connect, disconnect, subscribe, unsubscribe } = rosbridgeConnectionsSlice.actions;
export default rosbridgeConnectionsSlice.reducer;
