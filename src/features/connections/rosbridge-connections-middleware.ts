import { Middleware, MiddlewareAPI } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { connect } from './rosbridge-connections-slice';

const connectionsMiddleware: Middleware<
  {},
  RootState
> = store => {

  return next => action => {
    if (connect.match(action)) {
    }
  };
}

export default connectionsMiddleware;
