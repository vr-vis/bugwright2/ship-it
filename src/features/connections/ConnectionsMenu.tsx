
import PopoverListButton from "../../common/PopoverListButton";
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { RadioButtonChecked, RadioButtonUnchecked, Power, PowerOff } from '@mui/icons-material';
import { List, ListItem, ListItemButton, ListItemIcon, ListItemText } from "@mui/material";
import Show from "../../common/Show";
import { useStore } from '../../common/store';
import { useState } from "react";
import ConnectionInfoDialog from "./ConnectionInfoDialog";


const ConnectionMenu = () => {
  const connections = useStore(state => state.connections);
  const [selectedConnection, selectConnection] = useState<string>();

  return (
    <>
      <ConnectionInfoDialog
        connection={selectedConnection}
        onClose={() => { selectConnection(undefined) }}
      />
      <PopoverListButton
        icon={ Power }
        title="Connections"
      >
      {
        Object.keys(connections).map(uri => connections[uri]).map(connection => 
          <ListItem
            key={connection.uri}
          >
            <ListItemButton
              onClick={() => selectConnection(connection.uri)}
            >
              <ListItemIcon>
              {
                connection.status === 'disconnected' ?
                  <PowerOff color="error" /> : 
                  <Power color={connection.status === 'connected' ? "success" : "warning"} />
              }
              </ListItemIcon>
              <ListItemText>
                { connection.uri }
              </ListItemText>
            </ListItemButton>
          </ListItem>
        )
      }
      </PopoverListButton>
    </>
  );
};

export default ConnectionMenu;
