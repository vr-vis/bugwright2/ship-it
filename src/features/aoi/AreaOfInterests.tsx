import { createRef, PropsWithoutRef, useEffect, useId, useState } from 'react';
import { Box } from '@mui/system';
import { Button, Paper, List, ListItem, ListItemButton, MobileStepper, Select, MenuItem, FormControl, Slider, InputLabel, TextField, Input, InputAdornment, OutlinedInput, IconButton } from '@mui/material';
import { Add as AddIcon, Save as SaveIcon, Cancel as CancelIcon, Stop as StopIcon, FiberManualRecord as RecordButton, KeyboardArrowLeft, KeyboardArrowRight, Delete as DeleteIcon } from '@mui/icons-material';
import { AreaOfInterest, AreaOfInterestType, useStore } from '../../common/store';
import corrosion1 from './corrosion1.png';
import corrosion2 from './corrosion2.png';
import Dialog from '../../common/Dialog';
import { Split } from '@geoffcox/react-splitter';
import Viewport from '../viewport/Viewport';
import Viewport2D from '../viewport/Viewport2D';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';

interface AudioRecorderProps {
  close: (blob?: Blob) => void;
}

function AudioRecorder(props: AudioRecorderProps) {
  const [mediaRecorder, setMediaRecorder] = useState<MediaRecorder>();
  const [isRecording, setIsRecording] = useState(false);
  const [audioBlob, setAudioBlob] = useState<Blob>();
  const audio = createRef<HTMLAudioElement>();

  useEffect(() => {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
        const mediaRecorder = new MediaRecorder(stream);
        const chunks: Blob[] = [];
        mediaRecorder.ondataavailable = event => chunks.push(event.data);
        mediaRecorder.onstop = () => {
          setAudioBlob(new Blob(chunks, { type: "audio/ogg; codecs=opus" }));
          chunks.splice(0, chunks.length);
        };
        setMediaRecorder(mediaRecorder)
      });
    } else {
      console.error("getUserMedia not supported on the browser! Audio recording will not work.");
    }
  }, []);

  const startRecording = () => {
    if (!mediaRecorder || isRecording) {
      return;
    }
    mediaRecorder.start();
    setIsRecording(true);
  };

  const stopRecording = () => {
    if (!mediaRecorder || !isRecording) {
      return;
    }
    mediaRecorder.stop();
    setIsRecording(false);
    // const audioURL = window.URL.createObjectURL(audioBlob);
    // audio.current.src = audioURL;
    // audio.current.play();
  };

  useEffect(() => {
    if (audioBlob && audio.current) {
    }
  }, [audioBlob, audio]);

  return (
    <Paper
      sx={{
        display: "flex",
        flexDirection: "column",
        padding: "1em 0em",
      }}
    >
      New Recording:
      <Box
        component="div"
        sx={{
          display: "flex",
          flexDirection: "row",
        }}
      >
        {
          isRecording ?
          <IconButton 
            onClick={stopRecording}
          >
            <StopIcon />
          </IconButton>
        :
          <IconButton 
            disabled={!mediaRecorder}
            onClick={startRecording}
          >
            <RecordButton />
          </IconButton>
        }
        {
          audioBlob && !isRecording ?
            <>
              <audio
                style={{ flexGrow: 1 }}
                controls
                src={window.URL.createObjectURL(audioBlob)}
                ref={audio}
              />
              <IconButton 
                onClick={() => props.close(audioBlob)}
              >
                <AddIcon />
              </IconButton>
              <IconButton 
                onClick={() => props.close()}
              >
                <CancelIcon />
              </IconButton>
            </>
            : undefined
        }
      </Box>
    </Paper>
  );
}

interface AreaOfInterestEditorProps {
  open: boolean;
  aoi?: AreaOfInterest;
  close: (aoi?: AreaOfInterest) => void;
}

function AreaOfInterestEditor(props: AreaOfInterestEditorProps) {
  const [view, setView] = useState<"2d" | "3d">("2d");

  const [type, setType] = useState<AreaOfInterestType>("other");
  const [location, setLocation] = useState<[number, number, number]>([0, 0, 0])
  const [severity, setSeverity] = useState(0);
  const [note, setNote] = useState("");
  const [nextInspection, setNextInspection] = useState("");
  const [audioRecordings, setAudioRecordings] = useState<Blob[]>([]);

  const [isRecordingNewAudio, setIsRecordingNewAudio] = useState(false);

  const id = useId();

  useEffect(() => {
    setIsRecordingNewAudio(false);
    setType(props.aoi?.type || "other");
    setLocation(props.aoi?.location || [0, 0, 0]);
    setSeverity(props.aoi?.severity || 1);
    setNote(props.aoi?.note || "");
    setNextInspection(props.aoi?.nextInspection || "");
    setAudioRecordings(props.aoi?.audioRecordings || []);
  }, [props.open, props.aoi]);


  return (
    <Dialog
      open={props.open}
      close={() => props.close()}
      title={props.aoi ? `Area of Interest ID: ${props.aoi?.id}` : `New Area of Interest`}
      width="80%"
      height="80%"
    >
    <Split
      initialPrimarySize="65%"
    >
      <Box
        component="div"
        sx={{
          width: "100%",
          height: "100%",
          display: "flex",
          flexDirection: "column",
          alignItems: "stretch",
        }}
      >
        <Box
          component="div"
          sx={{
            flexGrow: 1,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <h2>Marked Area</h2>
          <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
            <Select
              value={view}
              onChange={event => setView(event.target.value as "2d" | "3d")}
            >
              <MenuItem value={"2d"}>2D</MenuItem>
              <MenuItem value={"3d"}>3D</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <Box
          component="div"
          sx={{
            flexGrow: 1,
          }}
        >
        {
          view === "3d" ? <Viewport /> : <Viewport2D />
        }
        </Box>
      </Box>
      <Box
        component="div"
        sx={{
          width: "100%",
          height: "100%",
          // boxSizing: 'border-box',
          // padding: '1em',
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        <Box
          component="div"
          sx={{
            display: 'flex',
            flexDirection: 'column',
            flexGrow: 1,
            padding: '1.5em',
            rowGap: '1em',
            overflow: 'auto',
          }}
        >
          <FormControl>
            <InputLabel id={`${id}-type-label`}>Type</InputLabel>
            <Select
              label="Type"
              labelId={`${id}-type-label`}
              value={type}
              onChange={event => setType(event.target.value as AreaOfInterestType)}
            >
              <MenuItem value={"biofouling"}>Biofouling</MenuItem>
              <MenuItem value={"corrosion"}>Corrosion</MenuItem>
              <MenuItem value={"metal-plate-deficiency"}>Metal Plate Deficiency</MenuItem>
              <MenuItem value={"other"}>Other</MenuItem>
            </Select>
          </FormControl>


          <Box
            component="div"
          >
            <InputLabel>Severity</InputLabel>
            <Slider
              id={`${id}-severity`}
              valueLabelDisplay="auto"
              step={1}
              marks={[1,2,3,4,5,6,7,8,9,10].map(value => ({ value, label: `${value}` }))}
              min={1}
              max={10}
              value={severity}
              onChange={(_, severity) => setSeverity(severity as number)}
            />
          </Box>

          <LocalizationProvider dateAdapter={AdapterMoment}>
            <FormControl>
              <DatePicker
                label="Next Inspection"
                value={nextInspection}
                onChange={value => {
                  // console.warn();
                  if (value) setNextInspection((value as any).format('YYYY-MM-DD'));
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </FormControl>
          </LocalizationProvider>

          <Box
            component="div"
            sx={{
              display: "flex",
              flexDirection: "row",
            }}
          >
            <FormControl>
              <InputLabel id={`${id}-location-label`}>Location</InputLabel>
              <OutlinedInput
                sx={{
                  flexGrow: 1,
                }}
                type="number"
                startAdornment={<InputAdornment position="start">X</InputAdornment>}
                label="Location"
                value={location[0]}
                onChange={event => setLocation([parseFloat(event.currentTarget.value), location[1], location[2]])}
              />
            </FormControl>
            <FormControl>
              <OutlinedInput
                sx={{
                  flexGrow: 1,
                }}
                type="number"
                startAdornment={<InputAdornment position="start">Y</InputAdornment>}
                value={location[1]}
                onChange={event => setLocation([location[0], parseFloat(event.currentTarget.value), location[2]])}
              />
            </FormControl>
            <FormControl>
              <OutlinedInput
                sx={{
                  flexGrow: 1,
                }}
                type="number"
                startAdornment={<InputAdornment position="start">Z</InputAdornment>}
                value={location[2]}
                onChange={event => setLocation([location[0], location[1], parseFloat(event.currentTarget.value)])}
              />
            </FormControl>
          </Box>
          <TextField
            label={"Notes"}
            multiline
            maxRows={4}
            value={note}
            onChange={event => setNote(event.currentTarget.value)}
          />


          <Box
            component="div"
            sx={{
              display: "flex",
              flexDirection: "column",
            }}
          >
            <InputLabel>
              Audio Recordings 
              <IconButton
                onClick={() => setIsRecordingNewAudio(true)}
              >
                <AddIcon />
              </IconButton>
            </InputLabel>
            {
              isRecordingNewAudio ? 
                <AudioRecorder
                  close={audioBlob => {
                    if (audioBlob) {
                      setAudioRecordings(audioRecordings => [...audioRecordings, audioBlob]);
                    }
                    setIsRecordingNewAudio(false);
                  }}
                />
                :
                undefined
            }
            {
              audioRecordings.map((recordingBlob, index) =>
                <Box
                  component="div"
                  key={recordingBlob.size ^ index}
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                  }}
                >
                  <audio
                    style={{
                      flexGrow: 1,
                    }}
                    src={window.URL.createObjectURL(recordingBlob)}
                    autoPlay={false}
                    controls
                  />
                  <IconButton 
                    onClick={() => {
                      audioRecordings.splice(index, 1);
                      setAudioRecordings([...audioRecordings]);
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                </Box>
              )
            }
            
          </Box>


        </Box>
        <Box
          component="div"
          sx={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          <Button
            sx={{
              flexGrow: 1,
            }}
            type="button"
            color="primary"
            onClick={() => props.close({ id: props.aoi ? props.aoi.id : Number.NaN, type, note, location, nextInspection, severity, audioRecordings })}
          >
            <SaveIcon/>&emsp;Save
          </Button>
          <Button
            sx={{
              flexGrow: 1,
            }}
            type="button"
            color="error"
            onClick={() => props.close()}
          >
            <CancelIcon />&emsp;Cancel
          </Button>
        </Box>
      </Box>
    </Split>
    </Dialog>
  );
}

interface AreaOfInterestItemProps {
  aoi: AreaOfInterest;
}

function AreaOfInterestItem(props: AreaOfInterestItemProps) {
  let imageCount = 2;
  let images = [corrosion1, corrosion2];
  const [activeImage, setActiveImage] = useState(0);
  const [editorOpen, setEditorOpen] = useState(false);
  const setAreaOfInterest = useStore(store => store.setAreaOfInterest);

  return (
    <>
      <AreaOfInterestEditor
        open={editorOpen}
        aoi={props.aoi}
        close={aoi => {
          if (aoi) {
            setAreaOfInterest(aoi);
          }
          setEditorOpen(false);
        }}
      />
      <ListItem
        sx={{
          width: "unset"
        }}
      >
        <ListItemButton
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "stretch",
          }}
          title={props.aoi.note}
          onClick={() => setEditorOpen(true)}
        >
          <b>#{ props.aoi.id }</b>
          <Box
            component="div"
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "stretch",
            }}
          >
            <Box
              component="div"
              style={{
                flex: "0 0 auto",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <img src={images[activeImage]} style={{ maxWidth: "150px", maxHeight: "150px" }} alt="Area of interest" />
              <MobileStepper
                steps={imageCount}
                position="static"
                activeStep={activeImage}
                sx={{
                  background: "transparent",
                }}
                nextButton={
                  <Button
                    size="small"
                    disabled={activeImage === imageCount - 1}
                    onMouseDown={event => event.stopPropagation()}
                    onClick={event => {
                      setActiveImage(activeImage => activeImage + 1);
                      event.stopPropagation();
                    }}
                  >
                    <KeyboardArrowRight />
                  </Button>
                }
                backButton={
                  <Button
                    size="small"
                    disabled={activeImage === 0}
                    onMouseDown={event => event.stopPropagation()}
                    onClick={event => {
                      setActiveImage(activeImage => activeImage - 1);
                      event.stopPropagation();
                    }}
                  >
                    <KeyboardArrowLeft />
                  </Button>
                }
              />
            </Box>
            <Box
              component="div"
              sx={{
                flexGrow: 1,
                whiteSpace: 'nowrap',
              }}
            >
              <Box component="div">
                <b>Type:</b> { props.aoi.type }
              </Box>
              <Box component="div">
                <b>Severity:</b> { props.aoi.severity } / 10
              </Box>
              <Box component="div">
                <b>Next Inspection:</b> { props.aoi.nextInspection }
              </Box>
              <Box component="div">
                <b>Note:</b> { props.aoi.note.length < 20 ? props.aoi.note : `${props.aoi.note.substring(0, 20)}...` }
              </Box>
              <Box component="div">
                <b>Audio Notes:</b> { props.aoi.audioRecordings.length }
              </Box>
            </Box>
          </Box>
        </ListItemButton>
      </ListItem>
    </>
  );
}

export interface AreaOfInterestsProps {
}

export default function AreaOfInterests(props: PropsWithoutRef<AreaOfInterestsProps>) {
  const areaOfInterests = useStore(s => s.aois);
  const setAreaOfInterest = useStore(s => s.setAreaOfInterest);
  const [editorOpen, setEditorOpen] = useState(false);

  return (
    <Paper
      sx={{
        width: '100%',
        height: '100%',
        padding: '1em',
        boxSizing: 'border-box',
        display: "flex",
        flexDirection: "column",
      }}
    >
      <AreaOfInterestEditor
        open={editorOpen}
        close={aoi => {
          if (aoi) {
            let id = 0;
            while (id in areaOfInterests) {
              ++id;
            }
            aoi.id = id;
            setAreaOfInterest(aoi);
          }
          setEditorOpen(false);
        }}
      />
      <h2>Area Of Interests</h2>
      <Box
        component="div"
        sx={{
          flexGrow: 1,
          minHeight: 0,
          overflowY: 'auto',
        }}
      >
        <List 
          sx={{
            display: "flex",
            flexWrap: "wrap",
            alignItems: "flex-start",
            flexDirection: "row",
          }}
        >
        {
          Object.values(areaOfInterests).map(aoi =>
            <AreaOfInterestItem
              key={aoi.id}
              aoi={aoi}
            />
          )
        }
        </List>
      </Box>
      <Button
        onClick={() => setEditorOpen(true)}
        sx={{
          textTransform: 'none',
        }}
      >
        <AddIcon /> Add Area of Interest
      </Button>
    </Paper>
  );
}
